<html>
<head>
  <style type="text/css">
    .ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass span,.ExternalClass td,img {line-height: 100%;}.ExternalClass p{line-height: 140%;}#outlook a {padding: 0;}.ExternalClass,.ReadMsgBody {width: 100%;}a,blockquote,body,li,p,table,td {-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;}table,td {mso-table-lspace: 0;mso-table-rspace: 0;}img {-ms-interpolation-mode: bicubic;border: 0;height: auto;outline: 0;text-decoration: none;}table {border-collapse: collapse !important;}#bodyCell,#bodyTable,body {height: 100% !important;margin: 0;padding: 0;font-family: ProximaNova, sans-serif;}#bodyCell {padding: 20px;}#bodyTable {width: 600px;}@font-face {font-family: ProximaNova;src: url(https://cdn.auth0.com/fonts/proxima-nova/proximanova-regular-webfont-webfont.eot);src: url(https://cdn.auth0.com/fonts/proxima-nova/proximanova-regular-webfont-webfont.eot?#iefix)format("embedded-opentype"),url(https://cdn.auth0.com/fonts/proxima-nova/proximanova-regular-webfont-webfont.woff) format("woff");font-weight: 400;font-style: normal;}@font-face {font-family: ProximaNova;src: url(https://cdn.auth0.com/fonts/proxima-nova/proximanova-semibold-webfont-webfont.eot);src: url(https://cdn.auth0.com/fonts/proxima-nova/proximanova-semibold-webfont-webfont.eot?#iefix)format("embedded-opentype"),url(https://cdn.auth0.com/fonts/proxima-nova/proximanova-semibold-webfont-webfont.woff) format("woff");font-weight: 600;font-style: normal;}@media only screen and (max-width: 480px) {#bodyTable,body {width: 100% !important;}a,blockquote,body,li,p,table,td {-webkit-text-size-adjust: none !important;}body {min-width: 100% !important;}#bodyTable {max-width: 600px !important;}#signIn {max-width: 280px !important;}}
  </style>
</head>
<body>
  <center>
    <table
    style='width: 600px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;font-family: "ProximaNova", sans-serif;border-collapse: collapse !important;height: 100% !important;'
    align="center"
    border="0"
    cellpadding="0"
    cellspacing="0"
    height="100%"
    width="100%"
    id="bodyTable"
    >
    <tr>
      <td
      align="center"
      valign="top"
      id="bodyCell"
      style='-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 20px;font-family: "ProximaNova", Helvetica,sans-serif;height: 100% !important;'
      >
      <div class="main">
        <p
        style="text-align: center;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%; margin-bottom: 30px;"
        >
        <img
        src="https://dollardonationclub.com/images/pig.png"
        width="50"
        alt="Dollar Donation Club"
        style="-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;"
        />
      </p>

      <p style="text-align: left; line-height: 145%; font-size: 16px; margin-bottom: 20px;">Hey there {{ $greeting }}!</p>

      @if ($metric > 0)
      <p style="text-align: left; line-height: 145%; font-size: 16px; margin-bottom: 20px;">Congrats on making your first donation, removing {{ $metric }} {{ $metric_text }}</p>
      @else
      <p style="text-align: left; line-height: 145%; font-size: 16px; margin-bottom: 20px;">Congrats on making your first donation!</p>
      @endif

      <p style="text-align: left; line-height: 145%; font-size: 16px; margin-bottom: 20px;">Go to your profile <a href="https://dollardonationclub.com/profile">here</a>.</p>

      <p style="text-align: left; line-height: 145%; font-size: 16px; margin-bottom: 20px;">For accessing your account in the future, your password is: <b>{{ $password }}</b>. To change your password, click “forgot password” at next login.</p>
        @if ($receipt)
      <p style="text-align: left; line-height: 145%; font-size: 16px; margin-bottom: 20px;">Your tax receipt is attached, and you'll receive one every month!</p>
        @endif
      <p style="text-align: left; line-height: 145%; font-size: 16px; margin-bottom: 20px;">Thank you for unifying humanity to make massive social impact.</p>

      <p style="text-align: left; line-height: 145%; font-size: 16px; margin-bottom: 20px;">We love you! 🙌 💛</p>

      <p style="text-align: left; font-size: 16px; line-height: 145%;">
        Seth Blaustein<br/>
        Founder, <a href="https://dollardonationclub.com">Dollar Donation Club</a>
      </p>

      <p style="text-align: left; line-height: 145%; font-size: 16px; margin-bottom: 15px;">PS - Do you know <strong>3 people</strong> who would give $1/month to social impact?</p>

      <p style="text-align: left; line-height: 145%; font-size: 16px;">Shoot 'em your Invitation link from <a href="https://dollardonationclub.com/profile">your profile</a> to start building your Impact Family!</p>
    </div>
  </td>
</tr>
</table>
</center>
</body>
</html>
