<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
 <head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1" name="viewport">
  <meta name="x-apple-disable-message-reformatting">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="telephone=no" name="format-detection">
  <title>New message</title>
  <!--[if (mso 16)]>
    <style type="text/css">
    a {text-decoration: none;}
    </style>
    <![endif]-->
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]-->
  <!--[if gte mso 9]>
<xml>
    <o:OfficeDocumentSettings>
    <o:AllowPNG></o:AllowPNG>
    <o:PixelsPerInch>96</o:PixelsPerInch>
    </o:OfficeDocumentSettings>
</xml>
<![endif]-->
  <style type="text/css">

    @font-face {
      font-family: 'Futura';
      src: url({{ storage_path('fonts/Futura-Medium.ttf') }}) format("truetype");
      font-weight: normal;
      font-style: normal;
    }

    @font-face {
      font-family: 'Futura';
      src: url({{ storage_path('fonts/Futura-Bold.ttf') }}) format("truetype");
      font-weight: bold;
      font-style: normal;
    }

    @font-face {
      font-family: 'Futura';
      src: src: url({{ storage_path('fonts/Futura-Medium-Italic.ttf') }}) format("truetype");
      font-weight: normal;
      font-style: italic;
    }

    @page { margin: 0px; }

    body{
      margin: 0px;
      padding:0;
      background-color: #FFE0EA;
      width:100%;
      font-family: Futura, Helvetica,sans-serif;
    }

    div {
        margin:0;
        margin-left: 15%;
        margin-right: 15%;
    }

    strong {
        font-weight: bold;
    }

    #header {
        text-align: center;
        margin-top: 5%;
    }

    #header h1 {
        margin: 0px;
        line-height: 42px;
        color: #333333;
        font-size: 32px;
        font-weight: bold;
    }

    #message {
        line-height:27px;
        color:#333333;
        font-size: 16px;
        font-family: Futura, Helvetica, sans-serif;
    }

    #smallprint {
        text-align: center;
    }

    #smallprint p {
        line-height: 12px;
        color: #333333;
        font-size: 9px;
        margin: 0;
        padding: 0;
        width: 560px;
    }

    explosive:after {
        content:url(https://dollardonationclub.com/images/receiptemoji.PNG);
        width: 40px;
        margin-left: 10px;
        transform: rotate(15deg);
    }

    #footer-trees {
        width: 100%;
        margin: 0px;
        position: absolute;
        bottom: 0px;
    }

    /* @media only screen and (max-width:600px) {p, ul li, ol li, a { line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } .es-header-body h1 a, .es-content-body h1 a, .es-footer-body h1 a { font-size:30px!important } .es-header-body h2 a, .es-content-body h2 a, .es-footer-body h2 a { font-size:26px!important } .es-header-body h3 a, .es-content-body h3 a, .es-footer-body h3 a { font-size:20px!important } .es-menu td a { font-size:16px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-content-body p, .es-content-body ul li, .es-content-body ol li, .es-content-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:16px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button, button.es-button { font-size:20px!important; display:block!important; border-width:10px 0px 10px 0px!important } .es-adaptive table, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } tr.es-desk-hidden, td.es-desk-hidden, table.es-desk-hidden { width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } tr.es-desk-hidden { display:table-row!important } table.es-desk-hidden { display:table!important } td.es-desk-menu-hidden { display:table-cell!important } .es-menu td { width:1%!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } } */
  </style>
  </head>
  <body>
  <div id="header">
     <img class="pig" src="https://dollardonationclub.com/images/pig.png" alt="A Flying Pig!" width="40" />
     <h1>Thanks for your Donation</p>
  </div>

  <div id="message">
    <p>Dearest {{ $greeting }},</p>
    <p><explosive>WOW. The planet is stoked right now!</explosive></p>
    <p>This is a receipt for your most gracious donation to The Flying Pig.</p>
    <p><strong>Organization:</strong> {{ $organization }}</p>
    <p><strong>Goal:</strong> {{ $goals }}</p>
    <p><strong>Donor Name:&nbsp;</strong>{{ $name }}</p>
    <p><strong>Amount: </strong>${{ number_format($amount, 2, '.', ',') }}</p>
    <p><strong>Donation Interval: </strong>{{ $frequency }}</p>
    <p><strong>Receipt #:</strong>&nbsp;{{ $transaction_id }}</p>
    <p><strong>Donated At:&nbsp;</strong>{{ $timestamp }}</p>
    <p>Thank you for being awesome!</p>
    - The Flying Pig Team
  </div>

  <div id="smallprint">
    <img src="https://i.imgur.com/noKGRmP.png" width="100%" alt="thin line break" />
    <p>The Flying Pig is a subsidiary and component fund of Legacy Global Foundation Inc, (Tax ID# 37-1440662). Legacy Global Foundation Inc (headquartered at 1423 S. Higley Rd., Suite 127, Mesa, AZ 85206), is a public charity as described in the Internal Revenue Code Sections 501(c)(3), 509(a)(1), and 170(b)(1)(A)(vi). Donations to 501(c)(3) organizations are tax-deductible as allowed by law. All money and property transferred to Legacy Global Foundation Inc is an irrevocable gift to charity and will not be part of the donor's estate or available to the donor's heirs. No goods or services were provided in exchange for this donation. Charities and Donor Advised Funds Are Not FDIC Insured - Are Not Bank Guaranteed - May Lose Value. This acknowledgement is for a single donation. Deductions on donations are prohibited from being applied twice. Donors may use year-end summary receipts issued after the year has closed for tax purposes, but not both. Donors are solely responsible for reporting their donations accurately to the IRS. As with any donation, you should consult your independent attorney, tax advisor, and investment manager for recommendations and before changing or implementing any nancial, tax, or estate planning strategy.</p>
  </div>

  <div id="footer-trees">
    <img src="https://i.imgur.com/azQR8O1.png" width="100%" />
  </div>
</body>
</html>
