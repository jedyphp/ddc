<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $rules
 * @property int    $created_at
 * @property int    $updated_at
 */
class Wallet extends Model
{

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id', 'rules'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'rules' => 'array'
    ];

    public $timestamps = true;

    public function member()
    {
        return $this->belongsTo('App\Wallet');
    }

    public function allocations()
    {
        return $this->hasMany('App\Allocation');
    }

    public function inflows()
    {
        return $this->hasMany('App\Inflow');
    }
}
