<?php

namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $appends = [
        'progress'
    ];

    public function metric()
    {
        $this->belongsTo('App\Metric');
    }

    public function project()
    {
        $this->belongsTo('App\Project');
    }

    public function checkAchievement($total = null)
    {
        if (is_null($total))
        {
            return DB::table('metric_project')
                    ->select(DB::raw('total >= ' . $this->target . ' as achieved'))
                    ->where('project_id','=',$this->project_id)
                    ->where('metric_id','=',$this->metric_id)
                    ->first()
                    ->achieved;
        }
        else
        {
            return $total >= $this->target;
        }
    }

    public function updateAchievement($total = null)
    {
        $this->attributes['achieved'] = $this->checkAchievement($total);
    }

    public function getAchievedAttribute($value)
    {
        return $this->checkAchievement();
    }

    public function getProgressAttribute()
    {
        $tags = ['p:'.$this->project_id];
        return Cache::tags($tags)->rememberForever('achievementStatus:'.$this->id, function() {
            return DB::table('metric_project')
                    ->where('project_id','=',$this->project_id)
                    ->where('metric_id','=',$this->metric_id)
                    ->pluck('total')
                    ->first();
        });
    }

}
