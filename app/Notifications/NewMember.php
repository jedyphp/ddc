<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NathanHeffley\LaravelSlackBlocks\Messages\SlackMessage;

class NewMember extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    public function toSlack($notifiable)
    {
        if ($notifiable->isMember()) {
            $channel = '#new-members';
            $headline = ':partying_face: A new *' . ucfirst($notifiable->type) . '* has joined Dollar Donation Club!';
            if (app()->environment() != 'production') {
                $headline = 'TEST TEST TEST '.$headline;
            }
            $titles = [
                '<https://dollardonationclub.com/members/'.ucfirst($notifiable->first_name).ucfirst($notifiable->last_name).'-'.$notifiable->id
                .'|'.$notifiable->full_name.'> <<mailto:'.$notifiable->email.'|'.$notifiable->email.'>>'
            ];
        } else {
            $channel = '#new-partner-members';
            $headline = ':rocket: A new *' . ucfirst($notifiable->type) . '* has joined Dollar Donation Club! :partying_face:';
            $titles = [
                '<https://dollardonationclub.com/partner/'.$notifiable->partner->slug . '|' . $notifiable->partner->name .'>',
                'managed by: <https://dollardonationclub.com/members/'.ucfirst($notifiable->first_name).ucfirst($notifiable->last_name).'-'.$notifiable->id
                .'|'.$notifiable->full_name.'> <<mailto:'.$notifiable->email.'|'.$notifiable->email.'>>'
            ];
        }

        // Figure out financial fields
        if ($notifiable->subscriptions->first()) {
            $sub_type = ucfirst($notifiable->subscriptions->first()->plan);
            $allocations = $notifiable->subscriptions->first()->allocation;
            $tip = $notifiable->subscriptions->first()->tip;
        } else {
            $sub_type = 'One Time';
            $allocations = $notifiable->transactions->first()->allocation;
            $tip = $notifiable->transactions->first()->tip;
        }

        $allocation_strings = [];
        foreach ($allocations as $goal => $amount) {
            $allocation = ' $'.(float) $amount.' ';
            switch ($goal) {
                case '3': $allocation .= ':deciduous_tree:'; break;
                case '4':
                case '7': $allocation .= ':ocean:'; break;
                default: $allocation .= ':question:'; break;
            }
            $allocation_strings[] = $allocation;
        }
        $subscription = $sub_type . ': '.implode(' +', $allocation_strings);
        $subscription .= ($tip > 0) ? ' + $'.$tip.' tip! :tada:' : ''; //' (no tip :cry:)';

        $via = $notifiable->transactions->first()->processor ?? 'UNKNOWN :exclamation:';
        if ($via == 'cliq') { $via = 'cliq (ACH)'; }

        $fields = [
            ['type' => 'mrkdwn', 'text' => '*Subscription*'], ['type' => 'mrkdwn', 'text' => '*Via*'],
            ['type' => 'plain_text', 'text' => $subscription], ['type' => 'plain_text', 'text' => $via]
        ];

        if ($notifiable->country == 'United States' || $notifiable->country == 'United States of America') {
            $fields = array_merge($fields,[
                ['type' => 'mrkdwn', 'text' => '*City*'], ['type' => 'mrkdwn', 'text' => '*State*'],
                ['type' => 'plain_text', 'text' => $notifiable->city], ['type' => 'plain_text', 'text' => $notifiable->state]
            ]);
        } else {
            $fields = array_merge($fields,[
                ['type' => 'mrkdwn', 'text' => '*Country*'],
                ['type' => 'plain_text', 'text' => $notifiable->country]
            ]);
        }

        return (new SlackMessage)
                ->to($channel)
                ->from('Dollar Donation Bot')
                ->image('https://dollardonationclub.com/favicon.png')
                ->content($headline)
                ->attachment(function ($attachment) use ($titles,$fields) {
                    foreach ($titles as $title) {
                        $attachment->block(function($block) use ($title) {
                            $block->type('section')
                                  ->text([
                                        'type' => 'mrkdwn',
                                        'text' => $title
                                  ]);
                        });
                    }
                    $attachment->block(function($block) use ($fields) {
                        $block->type('section')->fields($fields);
                    });
                });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
