<?php

namespace App\Http\Controllers\Api;

use App\Member;
use App\Leaderboard;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LeaderboardController extends Controller
{

    /**
     * Return a leaderboard object for the current member, filtered to scope
     *
     * @param \App\Member $member The member to show leaderboards for (implicitly passed by Laravel)
     * @param string|null $scope One of: 'global','country','state','city'. Default: 'global'
     * @return \Illuminate\Http\Response
     */
    public function showByLocation(Member $member, string $scope = 'global', $suffix = null, string $type = 'member')
    {
        $tags = ['leaderboard', 'm:'.$member->id];
        if (!is_null($suffix)) {
            $tags[] = strtolower($suffix);
        }

        return Cache::tags($tags)
                ->rememberForever('lb'.$suffix.'-'.$scope.'-m'.$member->id, function() use ($member,$scope,$suffix)
                {
                    $leaderboard = new Leaderboard($member, $suffix);
                    return $leaderboard->getLeaderboardByScope($scope)->forget(['_lft','_rgt','created_at','updated_at','status']);
                });
    }

    public function showByMemberLocation(Member $member, string $scope = 'global')
    {
        return $this->showByLocation($member, $scope);
    }

    public function showByMemberLocationMonthly(Member $member, string $scope = 'global')
    {
        return $this->showByLocation($member, $scope, 'Monthly');
    }

    public function showByPartnerLocation(Member $member, string $scope = 'global')
    {
        return $this->showByLocation($member, $scope, null, 'partner');
    }

    public function showByPartnerLocationMonthly(Member $member, string $scope = 'global')
    {
        return $this->showByLocation($member, $scope, 'Monthly', 'partner');
    }

    public function showGlobal($suffix = null, $type='member')
    {
        $tags = ['leaderboard'];
        if (!is_null($type)) {
            $tags[] = strtolower($type);
        }
        if (!is_null($suffix)) {
            $tags[] = strtolower($suffix);
        }
        
        return Cache::tags($tags)
                ->rememberForever('lb'.$suffix, function() use ($suffix, $type) {
                    $leaderboard = new Leaderboard(null, $suffix, $type);
                    return $leaderboard->getLeaderboard()->forget(['_lft','_rgt','created_at','updated_at','status']);
                });
    }

    public function showGlobalMembers()
    {
        return $this->showGlobal();
    }

    public function showGlobalMonthlyMembers()
    {
        return $this->showGlobal('Monthly');
    }

    public function showGlobalPartners()
    {
        return $this->showGlobal(null,'partner');
    }

    public function showGlobalMonthlyPartners()
    {
        return $this->showGlobal('Monthly','partner');
    }
}
