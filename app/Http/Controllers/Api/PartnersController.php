<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Partner;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PartnersController extends Controller
{

    /**
     * Show list of all partners
     */

    public function list()
    {
        return Cache::tags(['partners'])->rememberForever('partnerList',function()
        {
            return Partner::with('member','member.metrics')->get();
        });
    }

    public function fundSummary()
    {
        return Cache::tags(['partners'])->rememberForever('impactSummary', function() {
            $partners = Partner::where('impactAcceleratorTotal','>','impactAcceleratorUsed')->get();

            $summary = [
                'activePartners' => $partners->count(),
                'totalFundAllTime' => $partners->reduce(function ($carry, $partner) {
                    return $carry + $partner->impactAcceleratorTotal;
                },0),
                'totalFundUsed' => $partners->reduce(function ($carry, $partner) {
                    return $carry + $partner->impactAcceleratorUsed;
                },0),
                'totalFundRemaining' => $partners->reduce(function ($carry, $partner) {
                    return $carry + ($partner->impactAcceleratorTotal - $partner->impactAcceleratorUsed);
                }, 0),
                'perNewMember' => $partners->reduce(function ($carry, $partner) {
                    return $carry + 1; // replace with $partners->acceleratorSpeed
                }, 0),
                'allocation' => $partners->groupBy('valid_project_id')->map(function($project) {
                    return $project->reduce(function ($carry, $partner) {
                        return $carry + 1; // replace with $partners->acceleratorSpeed
                    }, 0);
                })
            ];

            return $summary;
        });
    }

    // public function adminUpdate(Request $request, Partner $partner)
    // {
    //     return $this->update($request, $partner, false);
    // }

    /**
     * Store a newly created Member on registration.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partner $partner
     * @return \App\Partner $partner
     */
    public function update(Request $request, Partner $partner)
    {
        $validationFields = [
            'name' => ['sometimes','max:255'],
            'message' => ['sometimes','max:255'],
            'description' => ['sometimes'],
            'project_id' => ['sometimes'],
            'photo' => ['sometimes','mimes:jpeg,jpg,png,gif','file','max:5120'],
        ];

        $currentUser = $request->user();

        if ($currentUser->tokenCan('admin.partners')) {
            $validationFields = array_merge($validationFields,[
                'donation_prizes' => ['sometimes','json'],
                'donation_direct' => ['sometimes','json'],
            ]);
        }
        if (!(isset($currentUser->partner) && $currentUser->partner->id == $partner->id)
            && !$currentUser->tokenCan('admin.partners'))
        {
            return response()->json(['message' => 'Unauthorized Partner'], 403);
        }

        $validatedData = $request->validate($validationFields);

        if (array_key_exists('photo',$validatedData))
        {
            $photo = $validatedData['photo'];
            unset($validatedData['photo']);
            $partner->setPhotoFromFile($photo);
        }

        $partner->fill($validatedData);
        $partner->save();

        Cache::tags(['partner:'.$partner->id])->flush();
        Cache::tags(['m:'.$partner->member->id])->flush();

        return $partner;
    }

    /**
     * Return the specified Member.
     *
     * @param  \App\Partner  $member
     * @return \App\Partner $partner
     */
    public function show(Partner $partner)
    {
        $tags = ['partner:'.$partner->id,'m:'.$partner->member->id];
        return Cache::tags($tags)
                ->rememberForever('memberWithMetrics',function() use ($partner)
                {
                    return $partner->load('member','member.metrics','member.subscriptions');
                });
    }

    // public function adminSetPhoto(Request $request, Partner $partner)
    // {
    //     return $this->setPhoto($request, $partner, false);
    // }

    public function setPhoto(Request $request, Partner $partner)
    {
        // HACK: disable security for now
        // if (!$request->user()->tokenCan('admin.partners') && !(isset($request->user()->partner) && $request->user()->partner == $partner))
        // {
        //     return response()->json(['message' => 'Unauthorized Partner'], 403);
        // }

        $validatedData = $request->validate([
            'photo' => ['required','mimes:jpeg,jpg,png,gif','file','max:5120'],
        ]);

        $partner->setPhotoFromFile($validatedData['photo']);
        $partner->save();

        return $partner;
    }

}
