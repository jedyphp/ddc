<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Member;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

class MembersVanityController extends Controller
{

    public function getSampleAvatars(int $count = 1)
    {
        if ($count<1) { $count = 1; }
        if ($count>100) { $count = 100; }

        return Member::whereNotNull('avatar_path')->inRandomOrder()->select('avatar_path')->limit(5)->get()->pluck('avatar_url');
    }

}
