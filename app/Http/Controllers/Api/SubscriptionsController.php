<?php

namespace App\Http\Controllers\Api;

use Exception;

use App\Member;
use App\Project;
use App\Subscription;
use App\Transaction;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessTransaction;

class SubscriptionsController extends Controller
{

    public function list(Request $request, Member $member)
    {
        if ($request->user()->id != $member->id && ! $request->user()->tokenCan('admin.partners'))
        {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        return $member->subscriptions;
    }

    public function store(Request $request, Member $member)
    {
        if ($request->user()->id != $member->id && ! $request->user()->tokenCan('admin.partners'))
        {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $validatedData = $request->validate([
            'subscription_data' => ['required'],
            'subscription_data.processor' => ['required',Rule::in(config('payments.processors'))],
            'subscription_data.amount' => ['required','numeric'],
            'subscription_data.tip' => ['required','numeric'],
            'subscription_data.processing_fee' => ['required','numeric'],
            'subscription_data.subscription_id' => ['required',
                                                    Rule::unique('subscriptions','subscription_id')->where(function($query) use ($request) {
                                                        return $query->where('processor','=',$request->input('subscription_data.processor'));
                                                    })],
            'subscription_data.plan' => ['required'],
            'subscription_data.processor_data' => ['sometimes'],
            'subscription_data.allocation' => ['sometimes',
                                                Transaction::getAllocationValidationRule('subscription_data.amount')
                                              ],

            'transaction_data' => ['required'],
            'transaction_data.processor' => ['required',Rule::in(config('payments.processors'))],
            'transaction_data.amount' => ['required','numeric'],
            'transaction_data.processing_fee' => ['required','numeric'],
            'transaction_data.tip' => ['required','numeric'],
            'transaction_data.transaction_id' => ['required',
                                                    Rule::unique('transactions','transaction_id')->where(function($query) use ($request) {
                                                        return $query->where('processor','=',$request->input('transaction_data.processor'));
                                                    })],
            'transaction_data.processor_data' => ['sometimes'],
            'transaction_data.allocation' => ['sometimes',
                                                Transaction::getAllocationValidationRule('transaction_data.amount')
                                             ]
        ]);

        $subscription = $member->subscriptions()->create([
            'subscription_id' => $validatedData['subscription_data']['subscription_id'],
            'plan' => $validatedData['subscription_data']['plan'],
            'amount' => $validatedData['subscription_data']['amount'],
            'processing_fee' => $validatedData['subscription_data']['processing_fee'],
            'tip' => $validatedData['subscription_data']['tip'],
            'status' => 'active',
            'processor' => $validatedData['subscription_data']['processor'],
            'processor_data' => $validatedData['subscription_data']['processor_data'] ?? $validatedData['subscription_data']
        ]);

        $subscription->pushToServices(true);

        try {
            $transaction = $subscription->createTransaction($validatedData['transaction_data']);

            $allocation_value = $validatedData['transaction_data']['allocation'] ?? [$member->defaultProjectId() => $validatedData['transaction_data']['amount']];

            // support for now old 2 types of allocation
            if (is_array($allocation_value) && !count($allocation_value)) {
                $project_id = $transaction->project_id;
            } else {
                $project_id = key($allocation_value);
            }

            // Change later to dynamic get allocation wallets
            $member->wallets[0]->allocations()->create([
                'project_id' => $project_id,
                'wallet_id' => $member->wallets[0]->id,
                'amount' => $validatedData['transaction_data']['amount'],
            ]);
        } catch (Exception $e) {
            $subscription->deleteWithEvent('transaction error');
            throw $e;
        }

        ProcessTransaction::dispatchSync($transaction, true);
        $subscription->member->resetCache();

        return $subscription;
    }

    public function update(Request $request, Member $member, Subscription $subscription)
    {

        if ($request->user()->id != $member->id && ! $request->user()->tokenCan('admin.partners'))
        {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $validatedData = $request->validate([
            'allocation' => ['required',
                            Transaction::getAllocationValidationRule($subscription->amount)
                            ],
        ]);

        $subscription->allocation = $validatedData['allocation'];
        $subscription->save();

        return $subscription;
    }

    public function delete(Request $request, Member $member, Subscription $subscription)
    {
        if ($request->user()->id != $member->id && ! $request->user()->tokenCan('admin.partners'))
        {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $subscription->deleteWithEvent('by member');
    }

    public function show(Request $request, Member $member, Subscription $subscription)
    {
        if ($request->user()->id != $member->id && ! $request->user()->tokenCan('admin.partners'))
        {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        if ($subscription->member_id == $member->id) {
            return $subscription;
        }
    }

}
