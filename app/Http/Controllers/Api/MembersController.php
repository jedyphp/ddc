<?php

namespace App\Http\Controllers\Api;

use Exception;

use App\Http\Controllers\Controller;
use App\Jobs\AddPartnerToAirtable;
use App\Jobs\ProcessTransaction;
use App\Jobs\UpdateProjectMetrics;
use App\Mail\WelcomeMessage;

use App\Member;
use App\MemberMetric;
use App\ImpactFamily;
use App\Partner;
use App\Project;
use App\Transaction;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Notifications\NewMember as DDCSlackMemberPush;

class MembersController extends Controller
{

    public function list(Request $request)
    {
        if (!$request->user()->tokenCan('admin.full')) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        return Member::all();
    }

    public function checkEmail(Request $request)
    {
        $validatedData = $request->validate(['email' => ['required','email']]);

        $member = Member::where('email','=',$validatedData['email'])->first();
        if ($member) {
            return response()->json(['errors'=>['email' => $member->type]]);
        }

        return response()->json(['result'=>'success']);
    }

    // Validate form for update or create without saving
    public function testValidation(Request $request, Member $member)
    {
        if ($member && $member->id != null) {
            return $this->update($request, $member, true);
        }
        return $this->store($request, true);
    }

    /**
     * Update an existing Member.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Member $member, $test = false)
    {

        $validatedData = $request->validate([
            'first_name' => ['sometimes','max:255'],
            'last_name' => ['sometimes','max:255'],
            'email' => ['sometimes','email',
                        Rule::unique('members', 'email')->ignore($member->id)->where(fn($query) => $query->whereNull('deleted_at'))],

            'password' => ['sometimes','confirmed','min:8'],
            // 'password_confirmed' field required for confirmation validation if password is required or present

            'country' => ['sometimes','max:255'],
            'street' => ['sometimes','max:255'],
            'city' => ['sometimes','max:255'],
            'state' => ['sometimes','max:255'],
            'postal_code' => ['sometimes','max:255'],
            'referrer_code' => ['sometimes'],
            'status' => ['sometimes','in:inactive,active'],
            'message' => ['sometimes','max:255'],

            'avatar' => ['sometimes','mimes:jpeg,jpg,png,gif','file','max:5120'],

        ]);

        // allow data validation without saving, by front-end dev request, in order to create a wizard format? dunno.
        if ($test) { return true; }

        if ($request->user()->id != $member->id && ! $request->user()->tokenCan('admin.partners'))
        {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $member->fill($validatedData);

        if (isset($avatar)) {
            $member->setAvatarFromFile($avatar);
        }

        $member->save();

        if ($member->type == 'partner')
        {
            Cache::tags(['partner:'.$member->partner->id])->flush();
            Cache::tags(['partners'])->flush();
        }

        $member->resetCache();

        $member->refresh();
        $member->pushToServices();

        return $this->fullCachedMember($member, 'Self');
    }

    /**
     * Store a newly created Member on registration.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $test = false)
    {

        $validatedData = $request->validate([
            'first_name' => ['required','max:255'],
            'last_name' => ['required','max:255'],
            'email' => ['required','email',Rule::unique('members', 'email')->where(fn($query) => $query->whereNull('deleted_at'))],
            'password' => ['required','confirmed','min:8'],
            // 'password_confirmed' field required for confirmation validation if password is required or present

            'country' => ['required','max:255'],
            'street' => ['sometimes','max:255'],
            'city' => ['required','max:255'],
            'state' => ['required','max:255'],
            'postal_code' => ['required','max:255'],
            'referrer_code' => ['sometimes'],
            'type' => ['sometimes','in:member,partner'],
            'status' => ['sometimes','in:inactive,active'],
            'message' => ['sometimes','max:255'],

            'subscription_data' => ['sometimes'],
            'subscription_data.processor' => ['required_with:subscription_data',Rule::in(config('payments.processors'))],
            'subscription_data.amount' => ['required_with:subscription_data','numeric'],
            'subscription_data.tip' => ['required_with:subscription_data','numeric'],
            'subscription_data.processing_fee' => ['required_with:subscription_data','numeric'],
            'subscription_data.subscription_id' => ['required_with:subscription_data',
                                                    Rule::unique('subscriptions','subscription_id')->where(function($query) use ($request) {
                                                        return $query->where('processor','=',$request->input('subscription_data.processor'));
                                                    })],
            'subscription_data.plan' => ['required_with:subscription_data'],
            'subscription_data.processor_data' => ['sometimes'],
            'subscription_data.allocation' => ['sometimes', Transaction::getAllocationValidationRule('subscription_data.amount')],

            'transaction_data' => ['required'],
            'transaction_data.processor' => ['required',Rule::in(config('payments.processors'))],
            'transaction_data.amount' => ['required','numeric'],
            'transaction_data.processing_fee' => ['required','numeric'],
            'transaction_data.tip' => ['required','numeric'],
            'transaction_data.transaction_id' => ['required',Rule::unique('transactions','transaction_id')->where(function($query) use ($request) {
                                                        return $query->where('processor','=',$request->input('transaction_data.processor'));
                                                    })],
            'transaction_data.processor_data' => ['sometimes'],
            'transaction_data.allocation' => ['sometimes', Transaction::getAllocationValidationRule('transaction_data.amount')],

            'avatar' => ['sometimes','mimes:jpeg,jpg,png,gif','file','max:5120'],

            'partner_name' => ['required_if:type,partner','max:255'],
            'partner_slug' => ['required_if:type,partner','unique:partners,slug','max:255'],
            'partner_message' => ['sometimes','max:255'],
            'partner_description' => ['sometimes','max:255'],
            'partner_anonymous' => ['sometimes','boolean'],
            'partner_projectid' => ['sometimes'],

            'send_welcome_email' => ['sometimes'],
        ]);

        // allow data validation without saving, by front-end dev request, in order to create a wizard format? dunno.
        if ($test) { return true; }

        if (array_key_exists('referrer_code',$validatedData) && !is_null($validatedData['referrer_code']))
        {
            $referrer = Member::firstWhere('referral_code',$validatedData['referrer_code']);
            unset($validatedData['referrer_code']);
        }

        if (array_key_exists('send_welcome_email', $validatedData) && $validatedData['send_welcome_email']) {
            $sendEmail = true;
            unset($validatedData['send_welcome_email']);
        } else {
            $sendEmail = false;
        }

        // set some default values
        $validatedData['partner_anonymous'] = $validatedData['partner_anonymous'] ?? '0';
        $validatedData['status'] = $validatedData['status'] ?? 'active'; // db sets to pending, so we explicitly make new members active

        if (array_key_exists('subscription_data', $validatedData) && !array_key_exists('processor', $validatedData['subscription_data'])) { $validatedData['subscription_data']['processor'] = 'nmi'; }
        if (!array_key_exists('processor', $validatedData['transaction_data'])) { $validatedData['transaction_data']['processor'] = 'nmi'; }

        // extract non-fillable values
        foreach ([
            'avatar' => 'avatar',
            'partner_name' => 'p_name',
            'partner_message' => 'p_message',
            'partner_description' => 'p_desc',
            'partner_slug' => 'p_slug',
            'partner_anonymous' => 'p_anon',
            'partner_projectid' => 'p_project',
        ] as $field => $name) {
            if (array_key_exists($field,$validatedData)) {
                $$name = $validatedData[$field] ?? null;
                unset($validatedData[$field]);
            }
        }

        $member = new Member;
        $member->fill($validatedData);

        // create member in the tree, in proper position
        if (isset($referrer) && !is_null($referrer)) {
            $member->appendToNode($referrer)->save();
            $referrer->resetCache();
        } else {
            $member->saveAsRoot();
        }
        $member->refresh();

        if ($member->type == 'partner') {
            $partner = $member->partner()->create([
                'name' => $p_name,
                'message' => $p_message,
                'description' => $p_desc,
                'slug' => $p_slug,
                'anonymous' => $p_anon,
                'project_id' => $p_project ?? Project::getValidAllocationIds()[0]
            ]);
            $member->refresh();
        }

        // Update referrals on ancestors
        $ancestor_ids = $member->ancestors()->pluck('id');
        if (!empty($ancestor_ids)) {
            Member::whereIn('id',$ancestor_ids)->update(['referrals' => DB::raw('referrals + 1'), 'referralsMonthly' => DB::raw('referralsMonthly + 1')]);
            foreach ($ancestor_ids as $ancestor_id) {
                Cache::tags(['m:'.$ancestor_id])->flush();
            }
        }

        // next, lets save their subscription and initial transaction information.
        if (isset($validatedData['subscription_data'])) {
            $subscription = $member->subscriptions()->create([
                'subscription_id' => $validatedData['subscription_data']['subscription_id'],
                'plan' => $validatedData['subscription_data']['plan'],
                'amount' => $validatedData['subscription_data']['amount'],
                'processing_fee' => $validatedData['subscription_data']['processing_fee'],
                'tip' => $validatedData['subscription_data']['tip'],
                'status' => 'active',
                'processor' => $validatedData['subscription_data']['processor'],
                'processor_data' => $validatedData['subscription_data']['processor_data'] ?? $validatedData['subscription_data'],
            ]);


            try {
                $transaction = $subscription->createTransaction($validatedData['transaction_data']);
                $subscription->pushToServices();
                $transaction->pushToServices();
            } catch (Exception $e) {
                $subscription->delete();
                throw $e;
            }

        } else {
            $validatedData['transaction_data']['processor_data'] = $validatedData['transaction_data']['processor_data'] ?? $validatedData['transaction_data'];
            $transaction = $member->transactions()->create($validatedData['transaction_data']);
        }

        $allocation_value = $validatedData['transaction_data']['allocation'] ?? [$member->defaultProjectId() => $validatedData['transaction_data']['amount']];

        // support for now old 2 types of allocation
        if (is_array($allocation_value) && !count($allocation_value)) {
            $project_id = $transaction->project_id;
        } else {
            $project_id = key($allocation_value);
        }

        // Change later to dynamic get allocation wallets
        $member->wallets[0]->allocations()->create([
            'project_id' => $project_id,
            'wallet_id' => $transaction->member->wallets[0]->id,
            'amount' => $validatedData['transaction_data']['amount'],
        ]);

        $transaction->load('member');
        ProcessTransaction::dispatchSync($transaction, true, false);

        // if member, accelerate impact, otherwise create the partner.
        if ($member->type == 'member') {
            // broke this out to private function due to complexity
            // $this->accelerateImpact(); // temporarily disabled
        } elseif ($member->type == 'partner') {
            // create one time point bonus for $referrer based on partner transaction, then flush referrer cache
            $amount = $transaction->amount + $transaction->processing_fee;
            if (isset($referrer) && is_a($referrer, Member::class)) {
                $referrer->gainPartnerBounty($amount);
                $referrer->resetCache();
            }

            Cache::tags(['partner:'.$member->partner->id])->flush();
            Cache::tags(['partners'])->flush();

            // trigger airtable addition
            try {
                $partner->refresh();
                AddPartnerToAirtable::dispatchSync($partner);
            } catch (Exception $e) {

            }
        }
        $receipt_pdf = ($member->isPartner() && $transaction->amount > config('payments.receipt.minimumTotal',1))
                              ? $transaction->getReceiptPDF() : null;


        // Finally, if creation came with an avatar, let's save that too.
        if (isset($avatar)) {
            $member->setAvatarFromFile($avatar);
        }

        $member->resetCache(); // probably unnecessary, but in case someone loads member before this function finishes.

        $member->refresh();
        $member->pushToServices();

        if ($sendEmail || App::environment(['local','develop'])) {
            // Send welcome message
            $greeting = $member->first_name;
            $password = $validatedData['password'];
            $metric = $member->metrics()->where('uid','oceanPlastic')->first()->progress->total ?? 0;

            Mail::to($member)->send(new WelcomeMessage($greeting, $password, $metric, null, $receipt_pdf));
        }


        try {
            if (app()->environment() == 'production') {
                $member->notify(new DDCSlackMemberPush());
            }
        } catch (Exception $e) {
            Log::error('Unable to push new member/partner to slack!',$e);
        }

        return $this->fullCachedMember($member, 'Self');
    }

    private function accelerateImpact()
    {
        $partners = Partner::with('member')->where(DB::raw('impactAcceleratorTotal - impactAcceleratorUsed','>=',1))->get();

        $project_ids = $partners->map(function($partner, $key) {
                            return $partner->valid_project_id;
                        })->unique();
        $projects = Project::whereIn('id',$project_ids)->get();

        foreach ($projects as $project) {

            $partnersForProject = $partners->filter(function($partner, $key) use ($project) {
                return $project->id == $partner->valid_project_id;
            });

            $value = $partnersForProject->reduce(function($carry,$partner) {
                return $carry + $partner->contribution;
            });

            $project->update([
                            'dollars' => DB::raw('dollars + ' . $value),
                            'dollarsMonthly' => DB::raw('dollarsMonthly + ' . $value)
                        ]);

            // NOTE: When $ai_increment is not hardcoded, update the below query to use the DB field
            Partner::whereIn('id',$partnersForProject->pluck('id'))->update('impactAcceleratorUsed',DB::raw('impactAcceleratorUsed + 1'));

            UpdateProjectMetrics::dispatchSync(
                $project->getAncestorIds(),
                $value
            );

            Cache::tags(['p:'.$project->id])->flush();
        }

        Cache::tags(['partners'])->forget('impactSummary');
    }

    private function fullCachedMember(Member $member, $kind = null)
    {
        $tags = ['m:'.$member->id];
        $key = 'memberWithMetrics' . $kind;
        return Cache::tags($tags)
                ->rememberForever($key,function() use ($member)
                {
                    if ($member->type == 'partner')
                    {
                        $member->load('partner');
                    }
                    return $member->load('metrics','subscriptions','transactions');
                });
    }


    public function family(Request $request, Member $member)
    {
        return $member->family;
    }

    /**
     * Return the specified Member.
     *
     * @param  \App\Member  $member
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Member $member)
    {
        if (auth('sanctum')->check() && $request->user()) {
            if ($request->user()->tokenCan('admin.partners'))
            {
                $member->makeVisible('permissions');
                $member->makeVisible('email_verified_at');
                return $this->fullCachedMember($member, 'Admin');
            } elseif ($request->user()->id == $member->id)
            {
                return $this->fullCachedMember($member, 'Self');
            }
        }

        $member->makeHidden('subscriptions');
        $member->makeHidden('transactions');
        $member->makeHidden('postal_code');
        $member->makeHidden('street');
        // $member->makeHidden('last_name');
        $member->makeHidden('email');

        return $this->fullCachedMember($member, 'Guest');
    }

    /** Return the currently authenticated user's member record */
    public function showSelf(Request $request)
    {
        return $this->show($request, $request->user());
    }

    public function setAvatar(Request $request, Member $member)
    {
        // FIXME: disabled avatar security due to weirdness during front end implementation
        // if (! ($request->user() == $member || $request->user()->tokenCan('admin.partners')))
        // {
        //     return response()->json(['message' => 'Unauthorized User: '.$request->user()->id.' != '.$member->id], 403);
        // }

        $validatedData = $request->validate([
            'avatar' => ['required','mimes:jpeg,jpg,png,gif','file','max:5120'],
        ]);

        $member->setAvatarFromFile($validatedData['avatar']);

        return $this->fullCachedMember($member);
    }


    public function updateDatastore(Request $request, Member $member)
    {
        // Only retrieve or store for current user
        if (!auth('sanctum')->check() || $request->user()->id != $member->id) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $validatedData = $request->validate([
            'key' => ['sometimes'],
            'data' => ['required'],
            'setfull' => ['sometimes','boolean'],
        ]);

        $setfull = (isset($validatedData['setfull']) && $validatedData['setfull']);

        if (isset($validatedData['key'])) {
            $data = $member->frontend_data;
            Arr::set($data, $validatedData['key'], $validatedData['data']);
            $member->frontend_data = $data;
        } elseif ($setfull) {
            $member->frontend_data = $validatedData['data'];
        } else {
            return response()->json(['message' => 'Bad Request: No Key Provided. setfull must be true to overwrite full data.'], 400);
        }

        $member->save();
        return $member->frontend_data;
    }

    public function getDatastore(Request $request, Member $member)
    {
        // Only retrieve or store for current user
        if (!auth('sanctum')->check() || $request->user()->id != $member->id) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $validatedData = $request->validate([
            'key' => ['sometimes']
        ]);

        if (isset($validatedData['key'])) {
            return data_get($member->frontend_data,$validatedData['key']);
        } else {
            return $member->frontend_data;
        }

        $member->save();
    }

}
