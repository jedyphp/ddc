<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    /**
     * Display a listing of projects.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Cache::tags(['p:all'])
                ->rememberForever('allProjectsWithMetricsAndAchievements',function() {
                    return Project::with(['metrics','achievements'])->get()->toTree();
                });
    }

    /**
     * Display the specified project object with children.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $descendant_tags = $project->descendants()->pluck('id')->map(function($id) { return 'p:'.$id; })->prepend('p:'.$project->id);
        return Cache::tags($descendant_tags)
                ->rememberForever('projectWithDescendantsAndMetricsAndAchievements',function () use ($project) {
                    return Project::whereDescendantOrSelf($project)->with(['metrics','achievements'])->get()->toTree();
                });
    }

}
