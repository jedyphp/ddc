<?php

namespace App\Http\Controllers\Api;

use App\Member;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function loginAsEmail(Request $request)
    {
        if (!$request->user()->tokenCan('admin.full')) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $currentUser = $request->user();

        $validatedData = $request->validate([
                            'email' => ['email','required','exists:members,email']
                        ]);

        $member = Member::where('email',$validatedData['email'])->with('partner')->first();

        $token = $member->createToken('auth-token', $member->permissions);

        return response()->json([
            'status_code' => 200,
            'access_token' => $token->plainTextToken,
            'token_type' => 'Bearer',
            'member' => $member
        ], 200);
    }

    public function logout(Request $request)
    {
        $validatedData = $request->validate([
                            'token' => ['sometimes']
                        ]);

        if (!array_key_exists('token',$validatedData) || $validatedData['token'] == 'current') {
            $request->user()->currentAccessToken()->delete();
        } elseif ($validatedData['token'] == 'all') {
            $request->user()->tokens()->delete();
        } else {
            $request->user()->tokens()->where('id',$validatedData['token'])->delete();
        }

        return response()->json([
            'status_code' => 200,
            'message' => 'Success'
        ], 200);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => ['email','required'],
            'password' => ['required']
        ]);

        $member = Member::where('email',$request->input('email'))->with('partner')->first();

        if ($member && empty($member->password)) {
            return response()->json([
                'status_code' => 401,
                'message' => 'User must set password'
            ], 401);
        }

        if (!$member || !Hash::check($request->input('password'), $member->password)) {
            return response()->json([
                'status_code' => 500,
                'message' => 'Unauthorized'
            ], 500);
        }

        $member->tokens()->delete();
        $token = $member->createToken('auth-token', $member->permissions);

        return response()->json([
            'status_code' => 200,
            'access_token' => $token->plainTextToken,
            'token_type' => 'Bearer',
            'member' => $member
        ], 200);
    }

    public function sendPasswordReset(Request $request)
    {
        $request->validate([
            'email' => ['required','email']
        ]);

        if ($request->has('mail_env')) {
            $newDomain = config('auth.password_reset_url'.strtolower($request->get('mail_env')),false);
            if ($newDomain) {
                config(['auth.password_reset_url' => $newDomain]);
            }
        }

        $status = Password::sendResetLink(
            $request->only('email')
        );

        if ($status == Password::RESET_LINK_SENT) {
            return response()->json([
                'status_code' => 200,
                'message' => 'Success'
            ], 200);
        } else {
            return response()->json([
                'status_code' => 500,
                'message' => 'Error: Failed To Send Reset Link'
            ], 500);
        }
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'token' => 'required',
            'email' => ['required','email'],
            'password' => ['required','confirmed','min:8']
        ]);

        // this requires the UserProvider to provide the proper user to the closure, so make sure that's configured properly
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function($member, $password) use ($request) {
                $member->forceFill([
                    'password' => $password // do not hash, because the member model will do it for us
                ])->save();

                $member->setRememberToken(Str::random(60));
            }
        );

        switch($status) {
            case Password::PASSWORD_RESET:
                return response()->json([
                    'status_code' => 200,
                    'message' => 'Password Reset'
                ], 200);
                break;
            case Password::INVALID_USER:
                return response()->json([
                    'status_code' => 500,
                    'message' => 'Error: Invalid User'
                ], 500);
                break;
            case Password::INVALID_TOKEN:
                return response()->json([
                    'status_code' => 500,
                    'message' => 'Error: Invalid Token'
                ], 500);
                break;
        }

        return response()->json([
            'status_code' => 500,
            'message' => 'Error: Reset Failed (Unknown)'
        ], 500);

    }


}
