<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessTransaction;
use App\Member;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function list(Request $request, Member $member) {
        if ($request->user()->id != $member->id && !$request->user()->tokenCan('admin.partners')) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        return $member->transactions()->with('subscription')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Member $member)
    {
        if ($request->user()->id != $member->id && !$request->user()->tokenCan('admin.partners')) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }
        $validatedData = $request->validate([
            'transaction_data' => ['required'],
            'transaction_data.processor' => ['required', Rule::in(config('payments.processors'))],
            'transaction_data.amount' => ['required', 'numeric'],
            'transaction_data.processing_fee' => ['required', 'numeric'],
            'transaction_data.tip' => ['required', 'numeric'],
            'transaction_data.transaction_id' => ['required',
                Rule::unique('transactions', 'transaction_id')->where(function ($query) use ($request) {
                    return $query->where('processor', '=', $request->input('transaction_data.processor'));
                })],
            'transaction_data.processor_data' => ['sometimes'],
            'transaction_data.allocation' => ['sometimes', Transaction::getAllocationValidationRule('transaction_data.amount')],
        ]);

        $transaction = $member->transactions()->create([
            'transaction_id' => $validatedData['transaction_data']['transaction_id'],
            'amount' => $validatedData['transaction_data']['amount'],
            'processing_fee' => $validatedData['transaction_data']['processing_fee'],
            'tip' => $validatedData['transaction_data']['tip'],
            'processor' => $validatedData['transaction_data']['processor'],
            'processor_data' => $validatedData['transaction_data']['procesor_data'] ?? $validatedData['transaction_data'],
        ]);

        $transaction->load('member');

        $allocation_value = $validatedData['transaction_data']['allocation'] ?? [$member->defaultProjectId() => $validatedData['transaction_data']['amount']];

        // support for now old 2 types of allocation
        if (is_array($allocation_value) && !count($allocation_value)) {
            $project_id = $transaction->project_id;
        } else {
            $project_id = key($allocation_value);
        }

        // Change later to dynamic get allocation wallets
        $member->wallets[0]->allocations()->create([
            'project_id' => $project_id,
            'wallet_id' => $transaction->member->wallets[0]->id,
            'amount' => $validatedData['transaction_data']['amount'],
        ]);

        ProcessTransaction::dispatchSync($transaction, true);
        $transaction->member->resetCache();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Member $member, Transaction $transaction)
    {
        if ($request->user()->id != $member->id && !$request->user()->tokenCan('admin.partners')) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        if ($transaction->member_id == $member->id) {
            return $transaction;
        }
    }

}
