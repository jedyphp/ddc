<?php

namespace App\Http\Controllers\Api;

use App\Update;
use App\Project;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class UpdatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        return $project->updates()->orderBy('number','DESC')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        if (!$request->user()->tokenCan('admin.updates')) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $validatedData = $request->validate([
            'title' => ['required','max:255'],
            'member_id' => ['required','exists:members,id'],
            'content' => ['sometimes'],
            'vimeo_id' => ['sometimes']
        ]);

        $validatedData['number'] = $project->updates()->max('number') + 1 ?? 1;

        $update = $project->updates()->create($validatedData);

        return $update;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Update  $update
     * @return \Illuminate\Http\Response
     */
    public function show(Update $update)
    {
        return $update;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Update  $update
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Update $update)
    {
        if (!$request->user()->tokenCan('admin.updates')) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $validatedData = $request->validate([
            'title' => ['sometimes','max:255'],
            'content' => ['sometimes'],
            'vimeo_id' => ['sometimes']
        ]);

        $update->fill($validatedData);

        $update->save();

        return $update;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Update  $update
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Update $update)
    {
        if (!$request->user()->tokenCan('admin.updates')) {
            return response()->json(['message' => 'Unauthorized User'], 403);
        }

        $update->delete();
    }
}
