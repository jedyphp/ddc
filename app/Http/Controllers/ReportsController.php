<?php

namespace App\Http\Controllers;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

use App\Member;
use App\Transaction;

use Carbon\Carbon;

class ReportsController extends Controller
{

    private function makeCsvFromResults(Collection $data, ?Collection $fields = null)
    {
        if ($fields) {
            $fields->map(fn($value) => (is_array($value)) ? $value[1] : $value);
        } else {
            $fields = collect(
                array_combine(
                    array_keys($data->first()->getAttributes()),
                    array_keys($data->first()->getAttributes())
                )
            );
        }

        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject);
        $csv->insertOne($fields->keys()->toArray());

        $data->each(function($item) use ($csv, $fields) {
            $csv->insertOne($item->getAttributes($fields->values()->toArray()));
        });

        return $csv;
    }

    public function sendCsvAsDownload(\League\Csv\Writer $csv, String $filename = 'report.csv')
    {
        return response()->streamDownload(function() use ($csv) { echo $csv->toString(); }, $filename, [
            "Content-Type" => "text/csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        ]);
    }

    public function wintogetherReferredAccounts(Request $request)
    {
        $validatedData = $request->validate([
            'date' => ['sometimes','date'],
            'display' => ['sometimes','boolean']
        ]);

        $fields = collect([
            'timestamp' => 'members.created_at',
            'email' => 'members.email',
            'donation' => 'members.total_donations'
        ]);

        $records = Member::query()->where('parent_id','=',1290)
                    ->when($validatedData['date'] ?? false, fn(Builder $query, $date) => $query->whereDate('members.created_at',$date));

        $fields->each(function($value, $key) use ($records) {
            $records->addSelect((is_array($value)) ? DB::raw($value[0].' as '.$value[1]) : $value);
        });

        $csv = $this->makeCsvFromResults($records->get(), $fields);

        if ($validatedData['display'] ?? false) {
            return $csv->toString();
        }

        $filename = 'ddc-wintogether-' . ($validatedData['date'] ?? 'all') . '.csv';

        return $this->sendCsvAsDownload($csv, $filename);
    }

    public function transactionLog(Request $request)
    {
        if (!($request->has('key') && $request->input('key') == env('REPORT_KEY'))) {
            if (!($request->user()->tokenCan('report.transactions') || $request->user()->tokenCan('admin.full')))
            {
                return response()->json(['message' => 'Unauthorized User'], 403);
            }
        }

        $validatedData = $request->validate([
            'start' => ['sometimes','date','before_or_equal:end'],
            'end' => ['sometimes','date','after_or_equal:start'],
            'from' => ['sometimes','integer','exists:transactions,id'],
            'to' => ['sometimes','integer','exists:transactions,id'],
            'member' => ['sometimes','integer','exists:members,id'],
            'subscription' => ['sometimes','integer','exists:subscriptions,id'],
            'processor' => ['sometimes',Rule::in(config('payments.processors'))],
            'processors' => ['sometimes'],
            'type' => ['sometimes','in:member,partner'],
            'limit' => ['sometimes','integer'],
            'display' => ['sometimes','boolean']
        ]);

        $fields = collect([
            'DDC Transaction ID' => 'transactions.id',
            'DDC Member ID' => 'transactions.member_id',
            'DDC Subscription ID' => 'transactions.subscription_id',
            'DDC Transaction Timestamp' => 'transactions.created_at',
            'Donation' => 'transactions.amount',
            'Tip' => 'transactions.tip',
            'Processing Fee' => 'transactions.processing_fee',
            'Processor' => 'transactions.processor',
            'Processor Transaction ID' => 'transactions.transaction_id',
            'Member Join Date' => ['members.created_at','member_created_at'],
            'Member Referrer ID' => ['members.parent_id','member_referrer_id'],
            'End of Week (Saturday)' => [DB::raw('DATE(transactions.created_at + INTERVAL (7 - DAYOFWEEK(transactions.created_at)) DAY)'),'transaction_weekending_sat'],
            'End of Month' => [DB::raw('LAST_DAY(transactions.created_at)'),'transaction_endofmonth'],
            'DDC Member Type' => 'members.type',
        ]);

        $transactions = Transaction::query()
                        ->when($validatedData['start'] ?? false, fn(Builder $query, $start) => $query->whereDate('transactions.created_at','>=',new Carbon($start)))
                        ->when($validatedData['end'] ?? false, fn(Builder $query, $end) => $query->whereDate('transactions.created_at','<=',new Carbon($end)))
                        ->when($validatedData['from'] ?? false, fn(Builder $query, $from) => $query->where('transactions.id','>=',$from))
                        ->when($validatedData['to'] ?? false, fn(Builder $query, $to) => $query->where('transactions.id','<=',$to))
                        ->when($validatedData['member'] ?? false, fn(Builder $query, $member_id) => $query->where('transactions.member_id',$member_id))
                        ->when($validatedData['subscription'] ?? false, fn(Builder $query, $subscription_id) => $query->where('transactions.subscription_id',$subscription_id))
                        ->when($validatedData['processor'] ?? false, fn(Builder $query, $processor) => $query->where('transactions.processor',$processor))
                        ->when($validatedData['processors'] ?? false, fn(Builder $query, $processor) => $query->whereIn('transactions.processor',array_filter(explode(',',$processor),fn($value) => in_array($value,config('payments.processors')))))
                        ->when($validatedData['type'] ?? false, fn(Builder $query, $type) => $query->where('members.type','=',$type))
                        ->join(with(new Member)->getTable(),'transactions.member_id','=','members.id')
                        ->orderBy('id','ASC');

        if (!isset($validatedData['limit']) || $validatedData['limit'] > 0) {
            $transactions->limit($validatedData['limit'] ?? 1000);
        }

        $fields->each(function($value, $key) use ($transactions) {
            $transactions->addSelect((is_array($value)) ? DB::raw($value[0].' as '.$value[1]) : $value);
        });

        $csv = $this->makeCsvFromResults($transactions->get(), $fields);

        if ($validatedData['display'] ?? false) {
            return $csv->toString();
        }

        return $this->sendCsvAsDownload($csv, 'ddc-transactions.csv');
    }
}
