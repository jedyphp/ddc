<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\WebhookClient\WebhookConfig;

use App\Webhooks\WebhookProcessor;

class WebhookController extends Controller
{
    public function __invoke(Request $request, WebhookConfig $config)
    {
        (new WebhookProcessor($request, $config))->process();

        return response()->json(['message' => 'ok']);
    }
}
