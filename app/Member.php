<?php
namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Laravel\Sanctum\HasApiTokens;

use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
// use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;

use App\Leaderboard;
use App\ImpactFamily;
use App\Traits\HasImpactLevelsTrait as HasImpactLevels;
use App\Traits\InteractsWithKlaviyoTrait as InteractsWithKlaviyo;

use Kalnoy\Nestedset\NodeTrait;

use App\Jobs\Klaviyo\UpdateKlaviyoProfile as UpdateKlaviyoProfileJob;

class Member extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    use HasApiTokens, Notifiable;
    use NodeTrait, HasApiTokens;

    use SoftDeletes;

    use HasImpactLevels, InteractsWithKlaviyo;

    protected static function booted()
    {
        static::created(function ($member) {
            $member->generateReferralCode();
            $member->save();
        });
    }

    /**
     * Route notifications for the Slack channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForSlack($notification)
    {
        return config('slack.webhook');
    }

    protected $casts = [
        'permissions' => 'array',
        'frontend_data' => 'array',
    ];

    protected $appends = [
        'ranks',
        'level',
        'level_next_at',
        'avatar_url',
        'impact_tier_bonus',
        'metric_levels',
        'points'
    ];

    protected $hidden = [
        '_lft',
        '_rgt',
        'permissions',
        'email_verified_at',
        'password',
        'remember_token',
        'name',
        'sub',
        'frontend_data'
    ];

    protected $fillable = [
        'type',
        'first_name',
        'last_name',
        'email',
        'password',
        'street',
        'city',
        'state',
        'postal_code',
        'country',
        'created_at',
        'status',
        'message'
    ];

    // set passwords as plaintext, hash automatically
    public function setPasswordAttribute(string $input): void
    {
        $this->attributes['password'] = Hash::make($input);
    }

    public function updates()
    {
        return $this->hasMany('App\Update');
    }

    public function partner()
    {
        return $this->hasOne('App\Partner');
    }

    public function subscriptions()
    {
        return $this->hasMany('App\Subscription');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function metrics()
    {
        return $this->belongsToMany('App\Metric')->using('App\MemberMetric')->as('progress')->withPivot('total');
    }

    public function getMetricLevelsAttribute()
    {
        return $this->metrics->mapWithKeys(function ($metric) {
            return [$metric->id => ['level' => $metric->progress->level, 'level_next_at' => $metric->progress->level_next_at, 'total_as_points' => $metric->progress->totalAsPoints()]];
        });
    }

    public function resetCache()
    {
        Cache::tags(['m:'.$this->id])->flush();
        if ($this->isPartner()) {
            Cache::tags(['partners'])->flush();
            Cache::tags(['p:'.$this->partner->id])->flush();
            Cache::tags(['p:'.$this->partner->id, 'm:'.$this->id])->flush();
        }
    }

    public function flushCache()
    {
        $this->resetCache();
    }

    public function getFamilyAttribute()
    {
        return ImpactFamily::find($this->id);
    }

    public function getProjectContributions()
    {
        return Project::whereIsLeaf()
                    ->join('metric_project', function($join) {
                        $join->on('projects.id' , '=', 'metric_project.project_id')
                                ->where('primary','=',1);
                        })
                    ->join('metrics', 'metric_project.metric_id', '=', 'metrics.id')
                    ->join('member_metric', 'metrics.id', '=', 'member_metric.metric_id')
                    ->where('member_metric.member_id','=',$this->id)
                    ->select('projects.id as project_id',
                                'metrics.uid as metric_slug',
                                'member_metric.total as metric_total',
                                DB::raw('member_metric.total / metric_project.dollarRatio as contribution'))
                    ->get();
    }

    public function defaultProjectId()
    {
        return ($this->isPartner()) ? $this->partner->valid_project_id : Project::getValidAllocationIds()[0];
    }

    public function generateReferralCode($bypass = false)
    {
        if (is_null($this->referral_code) || $bypass) {
            $code = strtoupper(
                str_rot13(
                    substr($this->first_name,0,1).substr($this->last_name,0,1)
                )
                .dechex($this->id + 100)
            );

            $characters = '0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
            while (strlen($code) < 6)
            {
                $code .= $characters[rand(0, strlen($characters) - 1)];
            }

            if ($bypass) {
                return $code;
            }

            $this->referral_code = $code;
        }

        return $this->referral_code;
    }

    public function isMember()
    {
        return $this->type == 'member';
    }

    public function isPartner()
    {
        return (($this->type == 'partner') && ($this->partner ?? false));
    }

    public function getRanksAttribute()
    {
        $lb = new Leaderboard($this, null, $this->type);
        return Cache::tags(['m:'.$this->id,'leaderboard'])->rememberForever('ranks',function() use ($lb) {
            return [
                'leaderboard' => $this->type,
                'city' => $lb->getRankByScope('city'),
                'state' => $lb->getRankByScope('state'),
                'country' => $lb->getRankByScope('country'),
                'global' => $lb->getRank(),
            ];
        });
    }

    public function getFamilySizeAttribute()
    {
        return $this->referrals;

        // this does include disabled accounts. not including them is more difficult.
        // return (($this->_rgt - $this->_lft - 1) / 2);
    }

    public function getImpactTierBonusAttribute()
    {
        $tier = 0;
        foreach (config('impact.bonus_tiers',['0'=>0]) as $nextTier => $family) {
            if ($family <= $this->family_size) {
                $tier = $nextTier;
            } else {
                break;
            }
        }
        return $tier;
    }

    public function gainPartnerBounty($amount)
    {
        $bounty = 0;
        foreach (config('impact.partner_bounties',['0'=>0]) as $bountyamount => $nextbounty) {
            if ($bountyamount <= $amount) {
                $bounty = $nextbounty;
            } else {
                break;
            }
        }
        if ($bounty > 0) {
            $this->update([
                'points' => DB::raw('points + '.$bounty),
                'pointsMonthly' => DB::raw('pointsMonthly + '.$bounty)
            ]);
            $this->refresh();
        }

        return $bounty;
    }

    public function getLevelAttribute()
    {
        $points = $this->points;
        return Cache::tags('m:'.$this->id)->rememberForever('level',function() use ($points) { return $this->getLevel($points); });
    }

    public function getLevelNextAtAttribute()
    {
        return $this->getLevelNextAt($this->level);
    }

    public function getAvatarUrlAttribute()
    {
        return (is_null($this->avatar_path)) ? null : Storage::url($this->avatar_path);
    }

    public function getFullNameAttribute()
    {
        return trim($this->first_name.' '.$this->last_name);
    }

    public function getPointsAttribute()
    {
        return $this->impacts()->sum('points');
    }

    public function setAvatarFromFile($file, $save = true)
    {
        $old_path = $this->avatar_path;
        $this->avatar_path = Storage::putFile(config('filesystems.disks.s3.folder_prefix','').'avatars',$file,['visibility' => 'public']);
        if (!is_null($old_path)) {
            Storage::delete($old_path);
        }
        if ($save) {
            $this->save();
            $this->resetCache();
        }
    }

    public function pushToServices($now = false)
    {
        if ($this->klaviyoEnabled()) {
            if ($now) {
                UpdateKlaviyoProfileJob::dispatchSync($this);
            } else {
                UpdateKlaviyoProfileJob::dispatch($this);
            }
        }
    }

    public function impacts() {
        return $this->hasMany('App\Impact');
    }

    public function wallets() {
        return $this->hasMany('App\Wallet');
    }
}
