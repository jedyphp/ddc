<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\Member as Member;
use stdClass;

class Leaderboard
{
    private $member;
    private $global;
    private $suffix;
    private $type;

    public function __construct(?Member $member, $suffix = null, $type = 'member')
    {
        $this->member = $member ?? null;
        $this->suffix = ucfirst($suffix);
        $this->global = is_null($member);
        $this->type = strtolower($type);
    }

    private function getQuery($modifyClosure = null)
    {
        $query = Member::where('type', $this->type)->whereIn('status',['active','past_due'])->oldest();

        if (!is_null($modifyClosure) && is_callable($modifyClosure)) {
            $modifyClosure($query);
        }

        return $query;
    }

    private function getModifierByLocation($country = null, $state = null, $city = null)
    {
        return function ($query) use ($country,$state,$city)
                {
                    if (!is_null($country)) {
                        $query->where('country','=',$country);
                    }

                    if (!is_null($state)) {
                        $query->where('state','=',$state);
                    }

                    if (!is_null($city)) {
                        $query->where('city','=',$city);
                    }
                };
    }

    private function getModifierByScope(string $scope)
    {
        switch ($scope) {
            case 'global':
                return function ($query) { return $query; };
                break;
            case 'country':
                return $this->getModifierByLocation($this->member->country);
                break;
            case 'state':
                return $this->getModifierByLocation($this->member->country, $this->member->state);
                break;
            case 'city':
                return $this->getModifierByLocation($this->member->country, $this->member->state, $this->member->city);
                break;
            default:
                throw new \Exception('Invalid Scope');
        }
    }

    public function getLeaderboard() {
        return $this->serializeCollection($this->getQuery()->get())->take(config('leaderboards.limit',20));
    }

    public function getLeaderboardByScope(string $scope) {
        return $this->serializeCollection($this->getQuery($this->getModifierByScope($scope))->get($this->getModifierByScope('city')))->take(config('leaderboards.limit',20));
    }

    private function getRankFromSubquery($collection)
    {
        return $this->serializeCollection($collection)->firstWhere('id', $this->member->id)->rank;
    }

    public function getRank()
    {
        return $this->getRankFromSubquery($this->getQuery()->get());
    }

    public function getRankByScope($scope = null)
    {
        return $this->getRankFromSubquery($this->getQuery($this->getModifierByScope($scope))->get());
    }

    private function serializeCollection($collect)
    {
        $collect->map(function($item) {
            $item->points = $item->points;
            $item->referals = $item['referrals'.$this->suffix];
        });
        $collect = $collect->sortByDesc('points')->values();
        $collect->map(function($item, $key) {
            $item->rank = $key + 1;
        });

        $new_collection = [];
        foreach ($collect->all() as $item) {
            $object = new stdClass();
            $object->id = $item->id;
            $object->rank = $item->rank;
            $object->points = $item->points;
            $object->first_name = $item->first_name;
            $object->last_name = $item->last_name;
            $object->referrals = $item->referrals;
            $object->created_at = $item->created_at;
            $object->city = $item->city;
            $object->state = $item->state;
            $object->country = $item->country;
            array_push($new_collection, $object);
        }

        return collect($new_collection);
    }
}
