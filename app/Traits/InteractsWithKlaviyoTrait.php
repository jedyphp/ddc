<?php

namespace App\Traits;

use App\Member as Member;

use Klaviyo\Klaviyo as Klaviyo;
use Klaviyo\Model\ProfileModel as KlaviyoProfile;
use Klaviyo\Model\EventModel as KlaviyoEvent;

trait InteractsWithKlaviyoTrait
{
    private $klaviyoInstance;
    private $enabled;

    public function klaviyoEnabled()
    {
        static $enabled = null;
        if (is_null($enabled)) {
            $enabled = array_reduce(config('services.klaviyo.envs'),
                                    fn($carry,$item) => (env($item,false) && $carry),
                                    true);
        }
        return $enabled;
    }

    private function klaviyoGetAPIInstance()
    {
        if (! $this->klaviyoInstance instanceof Klaviyo) {
            $this->klaviyoInstance = new Klaviyo( config('services.klaviyo.private_key'), config('services.klaviyo.public_key') );
        }
        return $this->klaviyoInstance;
    }

    private function klaviyoCallMethod($method, $data, $api = 'publicAPI')
    {
        if (!$this->klaviyoEnabled()) { return; }
        $this->klaviyoGetAPIInstance()->$api->$method($data);
    }

    private function klaviyoCustomerFromMember(Member $member)
    {
        // Gather Referrer Info
        $referrer = $member->parent;
        $referrer_properties = ($referrer instanceof Member) ?
            [
                '$was_referred' => 'true',
                '$referrer_name' => $referrer->full_name,
                '$referrer_first_name' => $referrer->first_name,
                '$referrer_last_name' => $referrer->last_name,
                '$referrer_email' => $referrer->email
            ] :
            [
                '$was_referred' => 'false'
            ];

        // Subscription Total
        $revenue =   $member->subscriptions
                            ->reduce(function($carry, $sub) {
                                    return [
                                            'amount' => $carry['amount'] + $sub->amount,
                                            'tip' => $carry['amount'] + $sub->amount,
                                    ];
                                },['amount' => 0, 'tip' => 0]
                            );

        $contributions = $member->getProjectContributions() ?? collect();
        $contribution = $contributions->mapWithKeys(fn($p) => ['$'.$p->metric_slug.'Contribution' => $p->contribution]);

        return array_merge([
            '$email' => $member->email,
            '$member_type' => $member->type,
            '$member_id' => $member->id,
            '$referral_code' => $member->referral_code,
            '$member_status' => $member->status,
            '$first_name' => $member->first_name,
            '$last_name' => $member->last_name,
            '$street_address' => $member->street,
            '$city' => $member->city,
            '$state' => $member->state,
            '$postalcode' => $member->postal_code,
            '$country' => $member->country,
            '$memberProfileSlug' => ucfirst($member->first_name) . ucfirst($member->last_name) . '-' . $member->id,
            '$partnerSlug' => ($member->isPartner()) ? $member->partner->slug : null,
            '$totalSubscription' => $member->subscriptions->sum('total'),
            '$totalSubscriptionDonations' => $member->subscriptions->sum('amount'),
            '$totalSubscriptionTips' => $member->subscriptions->sum('tip'),
            '$totalSubscriptionFees' => $member->subscriptions->sum('processing_fee'),
            '$totalPayments' => $member->transactions->sum('total'),
            '$totalPaymentsDonation' => $member->transactions->sum('amount'),
            '$totalPaymentsTip' => $member->transactions->sum('tip'),
            '$totalPaymentsFees' => $member->transactions->sum('processing_fee'),
            '$lastPaymentDate' => (string) $member->transactions->sortByDesc('created_at')->pluck('created_at')->first(),
            '$was_referred' => ($member->parent_id) ? 'true' : 'false'
        ], $referrer_properties, $contribution->toArray());

    }

    protected function klaviyoSendEvent(String $event, Array $customer, ?Array $properties)
    {
        $eventPayload = [
            'event' => $event,
            'customer_properties' => $customer
        ];
        if ($properties) {
            $eventPayload['properties'] = $properties;
        }

        $event = new KlaviyoEvent($eventPayload);

        $this->klaviyoCallMethod('track',$event);
    }

    protected function klaviyoSendCustomer(Array $customer)
    {
        $profile = new KlaviyoProfile($customer);

        $this->klaviyoCallMethod('identify',$profile);
    }

    protected function klaviyoDeleteProfile($email)
    {
        $this->klaviyoCallMethod('requestProfileDeletion',$email,'dataprivacy');
    }

    protected function klaviyoSendMemberEvent(Member $member, String $event, ?Array $properties)
    {
        $this->klaviyoSendEvent($event, $this->klaviyoCustomerFromMember($member), $properties);
    }

    protected function klaviyoSendMember(Member $member)
    {
        if ($member->trashed()) {
            $this->klaviyoDeleteProfile($member->email);
        } else {
            $this->klaviyoSendCustomer($this->klaviyoCustomerFromMember($member));
        }
    }

}
