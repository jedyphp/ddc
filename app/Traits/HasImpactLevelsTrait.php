<?php

namespace App\Traits;

trait HasImpactLevelsTrait
{

    private function getLevels()
    {
        $levels = config('impact.levels',[0=>0]);
        if (property_exists($this, 'levels')) {
            if (is_array($this->levels)) {
                return $this->levels;
            }
            return config($this->levels,$levels);
        }
        return $levels;
    }

    public function getLevel($points)
    {
        $level = 0;

        $levels = $this->getLevels();

        foreach ($levels as $nextlevel => $nextPoints) {
            if ($nextPoints > $points) {
                return $level;
            }
            $level = $nextlevel;
        }

        return $level;
    }

    public function getLevelNextAt($level)
    {
        $nextLevelIndex = $level + 1;

        $levels = $this->getLevels();

        if ($nextLevelIndex > sizeof($levels)) {
            return null;
        }
        return $levels[$nextLevelIndex];
    }

}
