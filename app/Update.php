<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Update extends Model
{

    protected $fillable = [
        'title',
        'number',
        'member_id',
        'content',
        'vimeo_id'
    ];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function author()
    {
        return $this->belongsTo('App\Member');
    }

}
