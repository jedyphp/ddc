<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property float  $amount
 * @property int    $created_at
 * @property int    $updated_at
 */
class Allocation extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'wallet_id', 'project_id', 'amount'
    ];

    public function wallet()
    {
        return $this->belongsTo('App\Wallet');
    }

    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
