<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Carbon\Carbon;

class WelcomeMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $greeting;
    public $metric;
    public $metric_text;
    public $password;
    public $receipt;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($greeting, $password, $metric, $metric_text = null, $receipt_pdf = null)
    {
        $this->greeting = $greeting;
        $this->password = $password;
        $this->metric = $metric;
        $this->metric_text = "lbs of Plastic from the ocean! 🌊";
        $this->receipt = $receipt_pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject('Holy crap welcome to Dollar Donation Club!')
             ->view('emails.welcome.member');

        if (!is_null($this->receipt) && is_array($this->receipt)) {
            $this->attachData($this->receipt['data'], $this->receipt['filename'], $this->receipt['options']);
        }

        return $this;
    }
}
