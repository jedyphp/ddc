<?php

namespace App\Mail;

use App\Transaction;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

use Carbon\Carbon;

class DonationReceipt extends Mailable
{
    use Queueable, SerializesModels;


    public $organization;
    public $greeting;
    public $name;
    public $goals;
    public $amount;
    public $frequency;
    public $transaction_id;
    public $timestamp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Transaction $transaction, $template = 'emails.donation.receiptattached')
    {

        $this->organization= 'The Flying Pig (Legacy Global)';
        $this->greeting = $transaction->member->first_name ?? 'Friend';
        $this->name = $transaction->member->full_name;
        $this->goals = $transaction->projects->implode('title',', ');
        $this->amount = $transaction->total;
        $this->frequency = ($transaction->subscription) ? 'Monthly' : 'One-time';
        $this->transaction_id = $transaction->id;
        $this->timestamp = $transaction->created_at . ' ' . $transaction->created_at->timezone;
        $this->pdf = $transaction->getReceiptPDF();
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view($this->template)
                    ->attachData($this->pdf['data'],
                                 $this->pdf['filename'],
                                 $this->pdf['options']);
    }
}
