<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $created_at
 * @property int $updated_at
 */
class Inflow extends Model
{

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wallet_id', 'transaction_id', 'amount'
    ];

    public $timestamps = true;

    public function wallet()
    {
        return $this->belongsTo('App\Wallet');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }

    public function impacts()
    {
        return $this->hasMany('App\Impact');
    }
}
