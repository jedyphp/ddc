<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\Log;

use App\Jobs\Klaviyo\SendKlaviyoEvent;

class Subscription extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'subscription_id',
        'plan',
        'amount',
        'processing_fee',
        'tip',
        'processor',
        'processor_data',
        'allocation',
        'wallet_distribution',
        'status'
    ];

    protected $casts = [
        'processor_data' => 'array',
        'allocation' => 'array',
        'wallet_distribution' => 'array'
    ];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function getTotalAttribute()
    {
        return $this->amount + $this->processing_fee + $this->tip;
    }

    public function createTransaction($transaction_data) {
        $transaction = $this->transactions()->create([
            'member_id' => $this->member->id,
            'amount' => $transaction_data['amount'] ?? $this->amount,
            'processing_fee' => $transaction_data['processing_fee'] ?? $this->processing_fee,
            'tip' => $transaction_data['tip'] ?? $this->tip,
            'processor' => $transaction_data['processor'] ?? $this->processor,
            'transaction_id' => $transaction_data['transaction_id'],
            'processor_data' => $transaction_data['processor_data'] ?? $transaction_data,
            'allocation' => $transaction_data['allocation'] ?? $this->allocation ?? [$this->member->defaultProjectId() => $transaction_data['amount']],
        ]);

        $transactionCount = $this->transactions()->count();
        if ($transactionCount > 1) {
            Log::info('Subscription Renewal: Transaction Created',[
                'member_id' => $this->member->id,
                'member_email' => $this->member->email,
                'subscription_id' => $this->id,
                'subscription_transaction_count' => $transactionCount,
                'transaction_id' => $transaction->id,
                'transaction_processor' => $transaction->processor
                ]);
        }

        return $transaction;
    }

    public function deleteWithEvent($event = null)
    {
        $member = $this->member;
        $member->refresh();

        $this->delete();

        $properties = [
            'reason' => $event ?? 'not provided'
        ];
        SendKlaviyoEvent::dispatchSync($this->member, 'Subscription Deleted', $properties);
    }

    public function pushToServices($now = false)
    {
        $method=($now) ? 'dispatchSync' : 'dispatch';

        $this->member->refresh();

        $properties = [
            'Payment Processor' => $this->processor,
            'Donation' => $this->amount,
            'Processing Fee' => $this->processing_fee,
            'Tip' => $this->tip,
            'Total' => $this->total,
            'Plan' => $this->plan,
            'DDC Subscription ID' => $this->id,
            'Payment Processor Subscription ID' => $this->subscription_id
        ];

        SendKlaviyoEvent::$method($this->member, 'Subscription Created', $properties);
    }

}
