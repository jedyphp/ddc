<?php
namespace App;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Laravel\Sanctum\HasApiTokens;

use Kalnoy\Nestedset\NodeTrait;

class ImpactFamily extends Model {

    use NodeTrait, SoftDeletes;

    protected $table = "members";
    protected $visible = [
        "id",
        "first_name",
        "last_name",
        "referrals",
        "points",
        "family",
        "metrics",
        "avatar_url"
    ];

    protected $appends = [
        "family"
    ];

    public function getFamilyAttribute()
    {
        return $this->children()->with('metrics')->get()->toTree();
    }

    public function getAvatarUrlAttribute()
    {
        return (is_null($this->avatar_path)) ? null : Storage::url($this->avatar_path);
    }

    public function metrics()
    {
        return $this->belongsToMany('App\Metric','member_metric','member_id','metric_id')->using('App\MemberMetric')->as('progress')->withPivot('total');
    }

}
