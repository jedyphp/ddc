<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Partner extends Model
{

    protected $fillable = [
        'name',
        'message',
        'description',
        'donation_prizes',
        'donation_direct',
        'slug',
        'anonymous',
        'project_id'
    ];

    protected $appends = [
        'photo_url',
    ];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function getValidProject()
    {
        return Project::find($this->valid_project_id);
    }

    public function getValidProjectIdAttribute($value)
    {
        if (is_null($this->project_id) || !in_array($this->project_id,Project::getValidAllocationIds()->toArray())) {
            return Project::getValidAllocationIds()[0];
        }
        return $this->project_id;
    }

    public function getPhotoUrlAttribute()
    {
        return (is_null($this->photo_path)) ? null : Storage::url($this->photo_path);
    }

    public function setPhotoFromFile($file)
    {
        $old_path = $this->photo_path;
        $this->photo_path = Storage::putFile(config('filesystems.disks.s3.folder_prefix','').'partner_photos',$file,['visibility' => 'public']);
        if (!is_null($old_path)) {
            Storage::delete($old_path);
        }
        $this->member->resetCache();
    }

    public function getContributionAttribute()
    {
        return 1;
    }

}
