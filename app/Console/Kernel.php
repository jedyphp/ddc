<?php

namespace App\Console;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Reset all monthly fields at 00:00 on the first day of the month
        $schedule->call(function () {
            foreach (config('schedule.zeroMonthly',[]) as $table => $field) {
                DB::table($table)->update(array_fill_keys((array) $field, 0));
            }
            foreach (config('schedule.nullMonthly',[]) as $table => $field) {
                DB::table($table)->update(array_fill_keys((array) $field, null));
            }
            Cache::tags(['monthly'])->flush();
        })->monthly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
