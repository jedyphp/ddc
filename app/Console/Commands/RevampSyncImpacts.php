<?php

namespace App\Console\Commands;

use App\Impact;
use App\Member;
use Illuminate\Console\Command;

class RevampSyncImpacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:impacts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse current data to the new database structure';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $members = Member::all();

        foreach ($members as $member) {
            $this->issueImpacts($member);
        }
    }

    public function issueImpacts($member)
    {
        if ($member->isMember()) {
            $generation = 0;

            $generation_modified = max(($generation - $member->impact_tier_bonus), 0);

            $transactions = $member->transactions()->get();
            $i = 1;

            foreach ($transactions as $transaction) {
                $base_points = $transaction->amount * config('impact.dollarRatio', 4);

                $points = $base_points * config('impact.generations.' . $generation_modified, config('impact.generations.default', '0'));

                if ($member->status == 'active' || $member->status == 'past_due') {
                    $inflows = $transaction->inflows()->get();
                    foreach ($inflows as $inflow) {
                        Impact::create([
                            'inflow_id' => $inflow->id,
                            'member_id' => $member->id,
                            'points' => $points,
                        ]);
                    }

                    if ($i == count($transactions) && !$member->isRoot() && $member->type != 'partner') {
                        $this->issueImpacts($member->parent()->withTrashed()->first(), $base_points, $generation + 1, true);
                    }
                }

                $i++;
            }
        } else {
            $transactions = $member->transactions()->get();

            foreach ($transactions as $transaction) {
                $points = $transaction->amount * config('impact.dollarRatio', 4) * config('impact.generations.0', '0');

                if ($member->status == 'active' || $member->status == 'past_due') {
                    $inflows = $transaction->inflows()->get();
                    foreach ($inflows as $inflow) {
                        Impact::create([
                            'inflow_id' => $inflow->id,
                            'member_id' => $member->id,
                            'points' => $points,
                        ]);
                    }
                }
            }
        }
    }
}
