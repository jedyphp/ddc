<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RevampSyncTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:transactions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse current data to the new database structure';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // STEP 1: Create wallets for all users

        $members = \App\Member::all();

        foreach ($members as $member) {

            $member->wallets()->create([
                'member_id' => $member->id,
            ]);
        }

        // STEP 2: Parse current transactions into wallets

        $transactions = \App\Transaction::all();

        foreach ($transactions as $transaction) {
            $transaction->inflows()->create([
                'transaction_id' => $transaction->id,
                'wallet_id' => $transaction->member->wallets[0]->id,
                'amount' => $transaction->amount,
            ]);
        }

        // STEP 3: Parse current allocations

        foreach ($transactions as $transaction) {
            $allocation_value = $transaction->allocation;
            if (is_array($allocation_value) && !count($allocation_value)) {
                $project_id = $transaction->project_id;
                $amount = $transaction->amount;
            } else {
                $project_id = key($allocation_value);
                $amount = $allocation_value[$project_id];
            }

            $transaction->member->wallets[0]->allocations()->create([
                'project_id' => $project_id,
                'wallet_id' => $transaction->member->wallets[0]->id,
                'amount' => $amount,
            ]);
        }
    }
}
