<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Member;

class MembersUpdateServices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'members:updateservices';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Push all members latest info to services';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $members = Member::all();
        $memberCount = $members->count();
        $count = 1;
        Member::all()->each(function(Member $member) use (&$count, $memberCount) {
            $member->pushToServices(true);
            $this->info($count . '/' . $memberCount . ' : Updated ' . $member->full_name);
            $count++;
        });
    }
}
