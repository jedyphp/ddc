<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Facades\Cache;

use App\Traits\HasImpactLevelsTrait as HasImpactLevels;

class MemberMetric extends Pivot
{
    use HasImpactLevels;

    protected $hidden = ['metric_id','member_id'];
    protected $appends = ['level','level_next_at'];
    public $timestamps = false;

    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    public function metric()
    {
        return $this->belongsTo('App\Metric');
    }

    public function totalAsPoints()
    {
        $ratio = Cache::tags('metric:'.$this->metric_id)->rememberForever('ratio',function() {
            $value = $this->metric->projects()->withPivot('dollarRatio')->wherePivotNotNull('dollarRatio')->pluck('dollarRatio')->first();
            unset($this->metric);
            return $value;
        });
        return floor(($this->total / $ratio) * config('impact.dollarRatio'));
    }

    public function getLevelAttribute()
    {
        return Cache::tags('m:'.$this->member_id)->rememberForever('metriclevel-'.$this->metric_id,function() {
            return $this->getLevel($this->totalAsPoints());
        });
    }

    public function getLevelNextAtAttribute()
    {
        return $this->getLevelNextAt($this->level);
    }
}
