<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $created_at
 * @property int $updated_at
 */
class Impact extends Model
{

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'inflow_id', 'member_id', 'points'
    ];

    public $timestamps = true;

    public function inflow()
    {
        return $this->belongsTo('App\Inflow');
    }

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
