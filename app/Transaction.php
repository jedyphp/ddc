<?php

namespace App;

use App\Member;
use App\Project;
use App\Subscription;
use App\Mail\DonationReceipt;
use Barryvdh\DomPDF\Facade as PDF;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;

use App\Jobs\Klaviyo\SendKlaviyoEvent;

class Transaction extends Model
{

    protected $fillable = [
        'member_id',
        'amount',
        'processing_fee',
        'tip',
        'transaction_id',
        'processor',
        'processor_data',
        'wallet_distribution'
    ];

    protected $casts = [
        'processor_data' => 'array',
        'allocation' => 'array',
        'wallet_distribution' => 'array'
    ];

    protected static function booted()
    {
        static::created(function ($transaction) {
            $sync = array_map(function($value) { return ['amount' => $value]; }, $transaction->allocation ?? []);
            $transaction->projects()->sync($sync);
        });
    }

    public static function getAllocationValidationRule($dataId) {
        return function($attribute, $value, $fail) use ($dataId) {

            $projectIds = Project::getValidAllocationIds()->toArray();
            $amount = (is_numeric($dataId)) ? $dataId : request($dataId);

            // must be array
            if (!is_array($value)) {
                return $fail($attribute.' is invalid.');
            }

            // must have proper amounts
            if (array_reduce($value,function($carry, $item) { return $carry + $item; },0) != $amount) {
                return $fail($attribute.' values must total to equal '.$dataId);
            }

            // must match existing ids
            $unknownKeys = array_diff(array_keys($value),$projectIds);
            if (!empty($unknownKeys)) {
                return $fail($attribute.' contains invalid or inactive project ids.');
            }
        };
    }

    public function setAllocationAttribute($value)
    {
        $value = $this->reviseAllocation($value);
        if ($value !== null && $this->isJsonCastable('allocation')) {
            $value = $this->castAttributeAsJson('allocation', $value);
        }
        $this->attributes['allocation'] = $value;
    }

    public function reviseAllocation($value)
    {
        $newAllocation = [];
        $validProjects = Project::getValidAllocationIds()->toArray();
        $total = 0;
        $defaultId = ($this->member) ? $this->member->defaultProjectId() : Project::getValidAllocationIds()[0];
        foreach ($value as $project_id => $amount) {
            if (!in_array($project_id, $validProjects)) {
                $project_id = $defaultId;
            }
            $newAllocation[$project_id] = $amount + ($newAllocation[$project_id] ?? 0);
            $total += $amount;
        }
        if ($total < $this->amount) {
            $newAllocation[$defaultId] = ($this->amount - $total) + ($newAllocation[$defaultId] ?? 0);
        }

        return $newAllocation;
    }

    public function member()
    {
        // in this case, use trashed, so transactions stay related when member is deleted
        return $this->belongsTo(Member::class)->withTrashed();
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class)->withTrashed();
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class)->withPivot('amount');
    }

    public function getTotalAttribute()
    {
        return $this->amount + $this->processing_fee + $this->tip;
    }

    public function pushToServices($now = false)
    {
        $method=($now) ? 'dispatchSync' : 'dispatch';

        $this->member->refresh();

        $properties = [
            'Type' => 'one-time',
            'Payment Processor' => $this->processor,
            'DDC Transaction ID' => $this->id,
            'Payment Processor Transaction ID' => $this->transaction_id,
            'Donation' => $this->amount,
            'Processing Fee' => $this->processing_fee,
            'Tip' => $this->tip,
            'Total' => $this->total,
        ];

        if ($this->subscription) {
            $properties['Type'] = 'subscription';
            $properties['Subscription Plan'] = $this->subscription->plan;
            $properties['Payment Processor Subscription ID'] = $this->subscription->subscription_id;
        }

        SendKlaviyoEvent::$method($this->member, 'Transaction Created', $properties);
    }

    public function sendReceipt($minimum = null)
    {
        $minimum = $minimum ?? config('payments.receipt.minimumTotal',1);
        if ($this->total < $minimum) {
            return false;
        }

        return Mail::to($this->member)
                ->bcc(config('payments.receipt.bcc',[]))
                ->send(new DonationReceipt($this));
    }

    public function getReceiptPDF($minimum = null)
    {
        $minimum = $minimum ?? config('payments.receipt.minimumTotal',1);
        if ($this->total < $minimum) {
            return false;
        }

        $data = [
            'organization' => config('payments.receipt.organization'),
            'greeting' => $this->member->first_name ?? 'Friend',
            'name' => $this->member->full_name,
            'goals' => $this->projects->implode('title',', '),
            'amount' => $this->total,
            'frequency' => ($this->subscription) ? 'Monthly' : 'One-time',
            'transaction_id' => $this->id,
            'timestamp' => $this->created_at . ' ' . $this->created_at->timezone,
        ];

        $pdf = PDF::loadView('pdf.donation.receipt',$data)->setPaper('legal','portrait');
        $filename = 'DDC Tax Receipt '.$this->created_at->toDateString().'.pdf';

        return [
            'data' => $pdf->output(),
            'filename' => $filename,
            'options' => ['mime' => 'application/pdf']
        ];
    }

    public function inflows()
    {
        return $this->hasMany('App\Inflow');
    }

}
