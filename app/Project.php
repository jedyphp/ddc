<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

use Kalnoy\Nestedset\NodeTrait;

class Project extends Model
{
    use NodeTrait;

    protected $casts = [
        'outcomes_primary' => 'json',
        'outcomes_secondary' => 'json',
        'sdg_numbers' => 'json'
    ];

    protected $hidden = [
        '_lft',
        '_rgt',
        'created_at',
        'updated_at'
    ];

    private static $validAllocationIds;

    public static function getValidAllocationIds()
    {
        if (empty(self::$validAllocationIds)) {
            self::$validAllocationIds = self::where('status','=','active')->isChild()->orderBy('created_at','desc')->pluck('id');
        }
        return self::$validAllocationIds;
    }

    public function updates()
    {
        return $this->hasMany('App\Update');
    }

    public function metrics()
    {
        return $this->belongsToMany('App\Metric')->using('App\MetricProject')->as('progress')->withPivot('total','target','dollarRatio','icon_url','primary','sort_index')->orderBy('sort_index','ASC');
    }

    public function achievements()
    {
        return $this->hasMany('App\Achievement');
    }

    public function addDollars($dollars)
    {
        if (!$this->is_child_goal) {
            throw new \Exception("Attempted to add dollars to non-Child Project");
        }

        $ancestor_ids = $this->ancestors()->pluck('id');

        // UPDATE projects set dollars = dollars + $dollars where id in (array_push($ancestor_ids,$this->id))
        // UPDATE metrics set dollars =
        // retrieve all ancestors with metrics, then update this and acenstors, adding the dollar value, and updating all metrics.
        // this could be done recursively, but that would be far worse DB wise.

        // also increment the daily amount added to this goal
    }

    public function getAncestorIds()
    {
        return Cache::rememberForever('p:'.$this->id.'-idWithAncestors',function () {
                                return $this->ancestors()->reversed()->pluck('id')->prepend($this->id);
                            });
    }

    /* Lots of scoping and convenience methods here to make these objects a bit cleaner */
    public function scopeIsMega($query)
    {
        return $query->whereIsRoot();
    }

    public function scopeIsChild($query)
    {
        return $query->whereIsLeaf();
    }

    public function getIsMegaGoalAttribute()
    {
        return $this->isRoot();
    }

    public function getIsChildGoalAttribute()
    {
        return $this->isLeaf();
    }

    public function getIsParentGoalAttribute()
    {
        return !($this->isRoot() || $this->isLeaf());
    }

    public function allocations()
    {
        return $this->hasMany('App\Allocation');
    }
}
