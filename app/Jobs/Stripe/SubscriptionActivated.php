<?php

namespace App\Jobs\Stripe;

use App\Subscription;
use Illuminate\Foundation\Bus\Dispatchable;
use Spatie\WebhookClient\Models\WebhookCall;

class SubscriptionActivated
{
    use Dispatchable;

    public $webhookCall;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $payload = $this->webhookCall->payload;
        $subscription = Subscription::where('subscription_id',[$payload->subscription])
                                              ->where('processor','stripe')
                                              ->first();

        $subscription->member()->update(['status' => 'active']);
        $subscription->update(['status' => 'active']);
    }
}
