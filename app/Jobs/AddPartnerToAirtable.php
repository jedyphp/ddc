<?php

namespace App\Jobs;

use App\Partner;

use Tapp\Airtable\Facades\AirtableFacade as Airtable;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddPartnerToAirtable implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $partner;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Partner $partner)
    {
        $this->partner = $partner;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $member = $this->partner->member;
        $transaction = $member->transactions()->first();

        $partner_id = [
            "Organization/Donor Name" => $this->partner->name
        ];
        $partner_values = [
            "POC Name" => $member->full_name,
            "Email" => $member->email,
            "Account Type" => ["Amplified Impact"],
            "Ampl Impact" => true,
            "$ amount AI" => (float) $transaction->amount,
            "Donation Frequency" => ($transaction->subscription_id) ? ["Monthly"] : ["One Time"],
            "Release Speed: X for each $1" => 1.0,
            "DDC Goal Funded" => $transaction->projects->implode('title',', '),
            "Anonymous" => (boolean) $this->partner->anonymous
        ];
        $record = Airtable::table('partners')->firstOrCreate($partner_id, $partner_values);

        $this->delete();
    }

}
