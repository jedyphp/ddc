<?php

namespace App\Jobs\NMI;

use App\Member;
use App\Subscription;
use App\Transaction;

use App\Jobs\ProcessTransaction;
use App\Webhooks\NMI\Traits\NMIHelperTrait;

use Illuminate\Support\Facades\Log;

use Spatie\WebhookClient\ProcessWebhookJob as SpatieProcessWebhookJob;

class TransactionSaleSuccess extends SpatieProcessWebhookJob
{
    use NMIHelperTrait;

    public function handle()
    {
        if ($this->isSubscriptionPayment()) {
            $subscription = $this->getMatchingSubscription();

            if (!$subscription) {
                Log::error('Subscription not found for webhook NMI\TransactionSaleSuccess', ['email' => $this->billingEmail(), 'amount'=>$this->amount(), 'date' => $this->transactionDate(), 'webhook_id' => $this->webhookCall->id]);
                return;
            }

            $event = $this->event();
            unset($event['billing_address']);
            unset($event['shipping_address']);
            unset($event['card']);

            $amount = $event['requested_amount'] - ($subscription->tip + $subscription->processing_fee);
            if ($amount < 0) {
                $amount = $event['requested_amount'];
            }

            $transaction = $subscription->createTransaction([
                'processor'=>'nmi',
                'transaction_id'=> $event['transaction_id'],
                'amount' => $amount,
                'processor_data' => $event
            ]);

            $subscription->status = 'active';
            $subscription->save();

            ProcessTransaction::dispatch($transaction);
            $this->delete();
        } else {
            // if not a subscription, it was processed on front end.
            $this->delete();
        }
    }
}
