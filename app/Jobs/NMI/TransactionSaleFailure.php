<?php

namespace App\Jobs\NMI;

use App\Member;
use App\Subscription;

use App\Jobs\ProcessTransaction;
use App\Webhooks\NMI\Traits\NMIHelperTrait;

use Illuminate\Support\Facades\Log;

use Spatie\WebhookClient\ProcessWebhookJob as SpatieProcessWebhookJob;

class TransactionSaleFailure extends SpatieProcessWebhookJob
{
    use NMIHelperTrait;

    public function handle()
    {

        if ($this->isSubscriptionPayment()) {
            $subscription = $this->getMatchingSubscription();

            if (!$subscription) {
                Log::error('Subscription not found for webhook transaction failure.', ['email' => $this->billingEmail(), 'amount'=>$this->amount(), 'date' => $this->transactionDate(), 'webhook_id' => $this->webhookCall->id]);
                return;
            }

            $subscription->update(['status' => 'past_due']);
            $subscription->member()->update(['status' => 'past_due']);
            $this->delete();
        } else {
            // if not a subscription, we got it from front end
            $this->delete();
        }

    }
}
