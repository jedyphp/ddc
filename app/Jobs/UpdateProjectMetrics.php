<?php

namespace App\Jobs;

use App\Project;
use App\Member;
use App\MetricProject;
use App\MemberMetric;
use App\Achievement;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateProjectMetrics implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $project_ids;
    protected $amount;
    protected $member;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(iterable $project_ids, float $amount, Member $member = null)
    {
        $this->project_ids = $project_ids;
        $this->amount = $amount;
        $this->member = $member;
    }

    private function updateProjectMetric($metric, $dollars)
    {
        $increase = $dollars * $metric->dollarRatio;

        $payload = [
            'total' => DB::raw('total + '.$increase),
            'totalMonthly' => DB::raw('totalMonthly + '.$increase)
        ];

        // Update project and parents for that metric
        MetricProject::whereIn('project_id', $this->project_ids)
                        ->where('metric_id',$metric->metric_id)
                        ->update($payload);
    }

    private function updateMemberMetric($metric, $dollars)
    {
        $increase = $dollars * $metric->dollarRatio;

        $payload = [
            'total' => DB::raw('total + '.$increase),
            'totalMonthly' => DB::raw('totalMonthly + '.$increase)
        ];

        // attach or update this metric on a member
        $updated = $this->member->metrics()->updateExistingPivot($metric->metric_id,$payload);
        if (!$updated) {
            $this->member->metrics()->attach($metric->metric_id,$payload);
        }
    }

    private function updateAchievements($metrics)
    {
        // Update all related project achievements at once, to save query time
        DB::table('achievements')
        ->leftJoin('metric_project',function ($join)
            {
                $join->on('achievements.project_id','=','metric_project.project_id')
                    ->on('achievements.metric_id','=','metric_project.metric_id');
            })
        ->whereIn('achievements.metric_id', $metrics->pluck('metric_id'))  // this line scopes to this metric set, without it all metrics update
        ->whereIn('achievements.project_id', $this->project_ids)  // this line scopes to this project set, without it all projects update
        ->where('achievements.achieved','=',0)
        ->where('metric_project.total', '>=', DB::raw('achievements.target'))
        ->update(['achievements.achieved'=>1]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $metrics = MetricProject::whereIn('project_id',$this->project_ids)->whereNotNull('dollarRatio')->get(['metric_id','dollarRatio']);

        foreach ($metrics as $metric) {
            // determine increment value
            $this->updateProjectMetric($metric,$this->amount);
            if (!is_null($this->member)) {
                $this->updateMemberMetric($metric,$this->amount);
            }
        }

        $this->updateAchievements($metrics);

        // Flush cache elements
        foreach ($this->project_ids as $project_id)
        {
            Cache::tags(['p:'.$project_id])->flush();
        }
        Cache::tags(['p:all'])->flush();

        if (!is_null($this->member)) {
            Cache::tags(['m:'.$this->member->id])->flush();
            Cache::tags(['m:all'])->flush();
        }

        $this->delete();
    }
}
