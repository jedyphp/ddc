<?php

namespace App\Jobs;

use App\Member;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class IssueImpactPoints implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $member;
    protected $generation;
    protected $base_points;
    protected $forceNow;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Member $member, $base_points, $generation = 0, $forceNow = false)
    {
        $this->member = $member;
        $this->generation = $generation;
        $this->base_points = $base_points;
        $this->forceNow = $forceNow;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $generation_modified = max(($this->generation - $this->member->impact_tier_bonus),0);

        $points = $this->base_points * config('impact.generations.'.$generation_modified, config('impact.generations.default','0'));

        //error_log('Updating '.$this->member->first_name.'('.$this->member->id.') with '.$points.' points');
        if ($this->member->status == 'active' || $this->member->status == 'past_due' )
        {
            Member::where('id',$this->member->id)->update([
                'points' => DB::raw('points + '.$points),
                'pointsMonthly' => DB::raw('pointsMonthly + '.$points)
            ]);
            $this->member->points += $points;
            $this->member->pointsMonthly += $points;
            $this->member->pushtoServices();
            Cache::tags(['m:'.$this->member->id])->flush();

        }

        if (!$this->member->isRoot() && $this->member->type != 'partner') {
            if ($this->forceNow) {
                self::dispatchSync($this->member->parent()->withTrashed()->first(), $this->base_points, $this->generation+1, true);
            } else {
                self::dispatch($this->member->parent()->withTrashed()->first(), $this->base_points, $this->generation+1);
            }
        } else {
            Cache::tags(['leaderboard'])->flush();
        }

        $this->delete();
    }
}
