<?php

namespace App\Jobs\Braintree;

use App\Subscription;

use App\Jobs\ProcessBraintreePayment;
use App\Jobs\ProcessTransaction;

use Spatie\WebhookClient\ProcessWebhookJob as SpatieProcessWebhookJob;
use App\Webhooks\Braintree\Traits\BraintreeGatewayTrait;

class SubscriptionCancelled extends SpatieProcessWebhookJob
{
    use BraintreeGatewayTrait;

    public function handle()
    {
        $webhookNotification = $this->parseBraintreeWebhookNotification($this->webhookCall->payload['bt_signature'],$this->webhookCall->payload['bt_payload']);
        $subscription = Subscription::where('subscription_id',$webhookNotification->subscription->id)->where('processor','=','braintree')->first();
        $subscription->member()->update(['status' => 'inactive']);
        $subscription->update(['status' => 'inactive']);
    }
}
