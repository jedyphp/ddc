<?php

namespace App\Jobs\Braintree;


use App\Member;
use App\Subscription;

use App\Jobs\ProcessBraintreePayment;
use App\Jobs\ProcessTransaction;

use Spatie\WebhookClient\ProcessWebhookJob as SpatieProcessWebhookJob;
use App\Webhooks\Braintree\Traits\BraintreeGatewayTrait;

use Illuminate\Support\Facades\Log;

class SubscriptionChargedSuccessfully extends SpatieProcessWebhookJob
{
    use BraintreeGatewayTrait;

    public function handle()
    {
        $webhookNotification = $this->parseBraintreeWebhookNotification($this->webhookCall->payload['bt_signature'],$this->webhookCall->payload['bt_payload']);

        $subscription = Subscription::where('subscription_id',$webhookNotification->subscription->id)
                                                ->where('processor','braintree')
                                                ->first();

        if (!$subscription) {
            Log::error('Subscription not found for webhook Braintree\SubscriptionChargedSuccessfully', [
                'subscription_id' => $webhookNotification->subscription->id,
                'subscription_object' => $webhookNotification->subscription,
                'webhook_id' => $this->webhookCall->id
                ]);
            return;
        }

        $bt_transaction = $webhookNotification->subscription->transactions[0]; // most recent is first

        $transaction = $subscription->createTransaction([
            'processor'=>'braintree',
            'transaction_id'=>$bt_transaction->id,
            'amount' => $bt_transaction->amount,
            'processor_data' => $bt_transaction
        ]);

        ProcessTransaction::dispatch($transaction);
    }
}
