<?php

namespace App\Jobs\Klaviyo;

use App\Member;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Traits\InteractsWithKlaviyoTrait as InteractsWithKlaviyo;

class SendKlaviyoEvent implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use InteractsWithKlaviyo;

    protected $member;
    protected $event;
    protected $properties;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Member $member, String $event, ?Array $properties)
    {
        $this->member = $member;
        $this->event = $event;
        $this->properties = $properties ?? null;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->klaviyoSendMemberEvent($this->member, $this->event, $this->properties);
    }
}
