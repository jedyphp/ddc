<?php

namespace App\Jobs\Klaviyo;

use App\Member;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Traits\InteractsWithKlaviyoTrait as InteractsWithKlaviyo;

class UpdateKlaviyoProfile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use InteractsWithKlaviyo;

    protected $member;
    protected $refresh;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Member $member, $refresh = false)
    {
        $this->member = $member;
        $this->refresh = $refresh;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->refresh) {
            $this->member->refresh();
        }
        $this->klaviyoSendMember($this->member);
    }
}
