<?php

namespace App\Jobs;

use App\Jobs\UpdateProjectMetrics;
use App\Jobs\IssueImpactPoints;

use App\Transaction;
use App\Project;
use App\Partner;
use App\MetricProject;
use App\MemberMetric;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $transaction;
    protected $now;
    protected $receiptEnabled;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Transaction $transaction, $now = false, $receiptEnabled = true)
    {
        $this->transaction = $transaction;
        $this->now = $now;
        $this->receiptEnabled = $receiptEnabled ?? true;
    }

    private function addDollarsToProjects($dollars, $project_ids)
    {
        Project::whereIn('id', $project_ids)
                    ->update([
                        'dollars' => DB::raw('dollars + ' . $dollars),
                        'dollarsMonthly' => DB::raw('dollarsMonthly + ' . $dollars)
                    ]);

        // Flush cache before worker dispatch
        foreach ($project_ids as $project_id)
        {
            Cache::tags(['p:'.$project_id])->flush();
        }
    }

    public function processPartnerTransaction()
    {
        DB::beginTransaction();

        if ($this->transaction->subscription) {
            $donation = $this->transaction->subscription->amount;
            $tip = $this->transaction->subscription->tip;
            $processing_fee = $this->transaction->subscription->processing_fee;
            if ($this->transaction->total < $donation+$tip+$processing_fee) {
                // The transaction is less than the amounts we are about to record. What do we do?
            }
            $this->transaction->subscription->status = 'active';
            $this->transaction->subscription->save();
        } else {
            $donation = $this->transaction->amount;
            $tip = $this->transaction->tip;
            $processing_fee = $this->transaction->processing_fee;
        }

        // First, increment partner member's total donations and points. we do not roll impact points up the tree for partners.
        $points = $donation * config('impact.dollarRatio',4) * config('impact.generations.0','0');
        $this->transaction->member()->update([
            'total_donations' => DB::raw('total_donations + ' . $donation),
            'total_tips' => DB::raw('total_tips + ' . $tip),
            'points' => DB::raw('points + ' . $points),
            'pointsMonthly' => DB::raw('pointsMonthly + '. $points)
        ]);

        $this->transaction->member->partner()->update([
            'impactAcceleratorTotal' => DB::raw('impactAcceleratorTotal + ' . $donation),
            'impactAcceleratorMonthly' => DB::raw('impactAcceleratorMonthly + ' . $donation)
        ]);

        // Next, allocate the transaction to the project and update all project totals
        $project_ids = $this->transaction->member->partner->getValidProject()->getAncestorIds();

        // MOSTLY ADAPTED FROM UpdateProjectMetrics.
        // Include *all* accelerator in metrics, not just disbersed.
        $metrics = MetricProject::whereIn('project_id',$project_ids)->whereNotNull('dollarRatio')->get(['metric_id','dollarRatio']);
        foreach ($metrics as $metric) {
            // determine increment value
            $increase = $donation * $metric->dollarRatio;
            $payload = [
                'total' => DB::raw('total + ' . $increase),
                'totalMonthly' => DB::raw('totalMonthly + ' . $increase)
            ];

            // attach or update this metric on a member
            $updated = $this->transaction->member->metrics()->updateExistingPivot($metric->metric_id,$payload);
            if (!$updated) {
                $this->transaction->member->metrics()->attach($metric->metric_id,$payload);
            }
        }

        Cache::tags(['m:'.$this->transaction->member->id])->flush();
        Cache::tags(['partner:'.$this->transaction->member->partner->id])->flush();
        Cache::tags(['partners'])->flush();

        // Finally, set the transaction to processed
        $this->transaction->processed = 1;
        $this->transaction->save();
        if ($this->receiptEnabled && $this->transaction->total >= config('payments.receipt.minimumTotal',1) && App::environment('production')) {
            $this->transaction->sendReceipt();
        }

        DB::commit();
    }

    public function processMemberTransaction()
    {
        DB::beginTransaction();

        if ($this->transaction->subscription) {
            $donation = $this->transaction->subscription->amount;
            $tip = $this->transaction->subscription->tip;
            $processing_fee = $this->transaction->subscription->processing_fee;
            if ($this->transaction->total < $donation+$tip+$processing_fee) {
                // The transaction is less than the amounts we are about to record. What do we do?
            }
            $this->transaction->subscription->status = 'active';
            $this->transaction->subscription->save();
        } else {
            $donation = $this->transaction->amount;
            $tip = $this->transaction->tip;
        }
        $impact_points = $donation * config('impact.dollarRatio',4);

        // First, increment user's total donations
        $this->transaction->member()->update([
            'total_donations' => DB::raw('total_donations + '.$donation),
            'total_tips' => DB::raw('total_tips + '.$tip)
        ]);
        Cache::tags(['m:'.$this->transaction->member->id])->flush();

        // Next, allocate the transaction to the project and update all project totals
        $allocation = $this->transaction->reviseAllocation($this->transaction->allocation);
        $projects = Project::whereIn('id', array_keys($allocation))->get();
        $sync = [];
        foreach ($projects as $project) {
            $ancestor_ids = $project->getAncestorIds();
            $amount = $allocation[$project->id];
            $sync[$project->id] = ['amount' => $amount];
            $this->addDollarsToProjects($amount, $ancestor_ids);

            // Update the metrics immediately (built as worker for future safety)
            UpdateProjectMetrics::dispatchSync(
                $ancestor_ids,
                $amount,
                $this->transaction->member
            );
        }
        $this->transaction->projects()->sync($sync);

        // Queue the Impact point updates (done async for speed)
        if ($this->now)
        {
            IssueImpactPoints::dispatchSync(
                $this->transaction->member,
                $impact_points,
                0,
                true
            );
        }
        else
        {
            IssueImpactPoints::dispatch(
                $this->transaction->member,
                $impact_points
            );
        }

        // Finally, set the transaction to processed
        $this->transaction->processed = 1;
        $this->transaction->save();
        if ($this->receiptEnabled && $this->transaction->total >= config('payments.receipt.minimumTotal',1) && App::environment('production')) {
            $this->transaction->sendReceipt();
        }

        DB::commit();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->transaction->processed) {
            if ($this->transaction->member->isPartner()) {
                $this->processPartnerTransaction();
            } else {
                $this->processMemberTransaction();
            }
            $this->transaction->pushToServices();
        }
        $this->delete();
    }
}
