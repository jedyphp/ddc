<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessBraintreePayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $notification;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($webhookNotification)
    {
        $this->notification = $webhookNotification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        error_log($this->notification->kind);
        // create transaction record, associate with user.
        // immediately execute worker to assign points for dollars to user, then queue worker to roll points up to parent with level
        // create worker to assign dollars from transaction to user's chosen child project and roll up other projects, then queue those projects to recalculate
    }
}
