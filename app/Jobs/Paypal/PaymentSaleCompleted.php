<?php

namespace App\Jobs\Paypal;

use App\Member;
use App\Subscription;
use App\Transaction;

use App\Jobs\ProcessTransaction;

use Illuminate\Support\Facades\Log;

use Spatie\WebhookClient\ProcessWebhookJob as SpatieProcessWebhookJob;

use App\Webhooks\Paypal\Traits\PaypalHelperTrait;

class PaymentSaleCompleted extends SpatieProcessWebhookJob
{
    use PaypalHelperTrait;

    public function handle()
    {
        if ($this->isSubscriptionPayment()) {
            $subscription = $this->getMatchingSubscription();

            if (!$subscription) {
                Log::error('Subscription not found for webhook Paypal\PaymentSaleCompleted', ['id' => $this->transactionId(), 'amount'=>$this->amount(), 'date' => $this->transactionDate(), 'webhook_id' => $this->webhookCall->id, 'paypal_data' => $this->paypalData()]);
                return;
            }

            if ($this->amount() != $subscription->total)
            {
                // amounts don't match!
            }

            if ($subscription->transactions()->where('transaction_id',$this->transactionId())->count() > 0) {
                Log::error('Paypal: Subscription already has matching transaction_id',['subscription_id' => $subscription->id, 'transaction_id' => $this->transactionId(), 'paypalData' => $this->paypalData()]);
                return;
            }

            if ($subscription->transactions->count() == 1 && $subscription->transactions->first()->transaction_id == $subscription->subscription_id) {
                $transaction = $subscription->transactions->first();

                $transaction->transaction_id = $this->transactionId();
                $transaction->processor_data =  [
                                                    'original' => $transaction->processor_data,
                                                    'paypal' => $this->paypalData()
                                                ];

                $transaction->save();
            } else {
                $transaction = $subscription->createTransaction([
                    'processor'=>'paypal',
                    'transaction_id'=> $this->transactionId(),
                    'amount' => $this->amount() - ($subscription->tip + $subscription->processing_fee),
                    'processor_data' => $this->paypalData()
                ]);
                ProcessTransaction::dispatch($transaction);
            }

            $subscription->status = 'active';
            $subscription->save();

            $this->delete();
        } else {
            // if not a subscription, it was processed on front end. Log just in case.
            Log::info('PayPal: Non-subscription Webhook Call Deleted',['payload' => $this->webhookCall->payload, 'resource' => $this->paypalData()]);
            $this->delete();
        }
    }
}
