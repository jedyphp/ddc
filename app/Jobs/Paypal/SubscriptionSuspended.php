<?php

namespace App\Jobs\Paypal;

use App\Member;
use App\Subscription;
use App\Transaction;

use App\Jobs\ProcessTransaction;
use App\Webhooks\Paypal\Traits\PaypalHelperTrait;
use Illuminate\Support\Facades\Log;

use Spatie\WebhookClient\ProcessWebhookJob as SpatieProcessWebhookJob;

class SubscriptionSuspended extends SpatieProcessWebhookJob
{
    use PaypalHelperTrait;

    public function handle()
    {
        $subscription = $this->getMatchingSubscription();

        if (!$subscription) {
            Log::error('Subscription not found for webhook Paypal\SubscriptionSuspended', ['email' => $this->paypalData()['subscriber']['email_address'], 'date' => $this->transactionDate(), 'webhook_id' => $this->webhookCall->id]);
            return;
        }

        $subscription->status = 'inactive';
        $subscription->save();

        $this->delete();
    }
}
