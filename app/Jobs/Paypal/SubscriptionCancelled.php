<?php

namespace App\Jobs\Paypal;

use App\Member;
use App\Subscription;
use App\Transaction;

use App\Jobs\ProcessTransaction;

use Illuminate\Support\Facades\Log;

use Spatie\WebhookClient\ProcessWebhookJob as SpatieProcessWebhookJob;

use App\Webhooks\Paypal\Traits\PaypalHelperTrait;

class SubscriptionCancelled extends SpatieProcessWebhookJob
{
    use PaypalHelperTrait;

    public function handle()
    {
        $subscription = $this->getMatchingSubscription();

        if (!$subscription) {
            Log::error('Subscription not found for webhook Paypal\SubscriptionCancelled', ['email' => $this->paypalData()['subscriber']['email_address'], 'date' => $this->transactionDate(), 'webhook_id' => $this->webhookCall->id]);
            return;
        }

        $subscription->deleteWithEvent('removed by paypal');

        $this->delete();
    }
}
