<?php

namespace App\Webhooks\Paypal\Traits;

use Exception;

use App\Subscription;

use GuzzleHttp\Client as GuzzleClient;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait UsesPaypalAPI
{
    private function paypalEndpointURI($endpoint)
    {
        return rtrim(config('payments.paypal.api_base'),'/') . '/' . ltrim($endpoint,'/');
    }

    private function paypalAccessToken()
    {
        $cacheKey = 'paypal-api-access-token';
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }

        $client = new GuzzleClient();
        $response = $client->request('POST', $this->paypalEndpointURI('oauth2/token'), [
                'headers' =>
                    [
                        'Accept' => 'application/json',
                        'Accept-Language' => 'en_US',
                        'Content-Type' => 'application/x-www-form-urlencoded',
                    ],
                'auth' => [config('payments.paypal.client_id'), config('payments.paypal.secret'), 'basic'],
                'body' => 'grant_type=client_credentials',
            ]);
        $data = json_decode($response->getBody(), true);

        $token = $data['access_token'];
        $expirationInSeconds = $data['expires_in'] - 60; // we remove 60 seconds to account for lag

        Cache::put($cacheKey,$token,$expirationInSeconds);
        return $token;
    }


    protected function paypalVerifySignature(Request $request, $webhook_id): bool
    {
        $verify = [
            'transmission_id' => $request->header('PAYPAL-TRANSMISSION-ID', ''),
            'transmission_time' => $request->header('PAYPAL-TRANSMISSION-TIME', ''),
            'cert_url' => $request->header('PAYPAL-CERT-URL', 'cert_url'),
            'auth_algo' => $request->header('PAYPAL-AUTH-ALGO', ''),
            'transmission_sig' => $request->header('PAYPAL-TRANSMISSION-SIG', ''),
            'webhook_id' => $webhook_id,
            'webhook_event' => json_decode($request->getContent(), true)
        ];

        try {
            $client = new GuzzleClient;
            $response = $client->request('POST', $this->paypalEndpointURI('notifications/verify-webhook-signature'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$this->paypalAccessToken()
                ],
                'body' => json_encode($verify)
            ]);

            if ($response->getStatusCode() == 200) {
                $message = json_decode((string) $response->getBody());
                if (property_exists($message,'verification_status') && $message->verification_status == 'SUCCESS') {
                    return true;
                }
                Log::error('Paypal Webhook Verification 200 Response But Not Success?!', ['response' => $response, 'message' => $message]);
            }

        } catch (Exception $e) {

        }

        return false;
    }

}
