<?php

namespace App\Webhooks\Paypal\Traits;

use Exception;

use App\Subscription;

use GuzzleHttp\Client as GuzzleClient;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

trait PaypalHelperTrait
{
    protected function paypalData()
    {
        return $this->webhookCall->payload['resource'];
    }

    protected function isSubscriptionPayment()
    {
        return ($this->webhookCall->payload['event_type'] == 'PAYMENT.SALE.COMPLETED' && array_key_exists('billing_agreement_id',$this->paypalData()));
    }

    protected function getMatchingSubscription()
    {
        switch ($this->webhookCall->payload['event_type']) {
            case 'PAYMENT.SALE.COMPLETED':
                $sub_id_key = 'billing_agreement_id';
                break;
            case 'BILLING.SUBSCRIPTION.SUSPENDED':
            case 'BILLING.SUBSCRIPTION.CANCELLED':
            case 'BILLING.SUBSCRIPTION.PAYMENT.FAILED':
                $sub_id_key = 'id';
                break;
            default:
                $sub_id_key = 'billing_agreement_id';
        }
        return (array_key_exists($sub_id_key,$this->paypalData())) ? Subscription::where('subscription_id',$this->paypalData()[$sub_id_key])->first() : null;
    }

    protected function transactionId()
    {
        return $this->paypalData()['id'];
    }

    protected function transactionDate()
    {
        return $this->paypalData()['create_time'];
    }

    protected function amount()
    {
        return $this->paypalData()['amount']['total'];
    }

}
