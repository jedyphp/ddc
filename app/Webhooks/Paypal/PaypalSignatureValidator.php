<?php

namespace App\Webhooks\Paypal;

use Exception;

use App\Webhooks\Paypal\Traits\UsesPaypalAPI;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use Spatie\WebhookClient\WebhookConfig;
use Spatie\WebhookClient\Exceptions\WebhookFailed;
use Spatie\WebhookClient\SignatureValidator\SignatureValidator as BaseValidator;

class PaypalSignatureValidator implements BaseValidator
{
    use UsesPaypalAPI;

    public function isValid(Request $request, WebhookConfig $config): bool
    {
        return $this->paypalVerifySignature($request, $config->signingSecret);
    }

}
