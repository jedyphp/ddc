<?php

namespace App\Webhooks;

use Illuminate\Support\Arr;
use Spatie\WebhookClient\WebhookConfig as SpatieWebhookConfig;

class WebhookConfig extends SpatieWebhookConfig
{
    public array $typeJobs;
    public $typeField;

    public function __construct(SpatieWebhookConfig $config)
    {
        $this->name = $config->name;

        $this->signingSecret = $config->signingSecret;

        $this->signatureHeaderName = $config->signatureHeaderName;

        $this->signatureValidator = $config->signatureValidator;

        $this->webhookProfile = $config->webhookProfile;

        $this->webhookResponse = $config->webhookResponse;

        $this->webhookModel = $config->webhookModel;

        $this->processWebhookJobClass = $config->processWebhookJobClass;

        $n = $this->name;
        $c = Arr::first(config('webhook-client.configs',[]),function($value, $key) use ($n) {
            return (is_array($value) && array_key_exists('name', $value) && $value['name'] == $n);
        });

        if ($c) {
            $this->typeJobs = $c['type_jobs'] ?? [];
            $this->typeField = $c['type_field'] ?? '';
        }
    }
}
