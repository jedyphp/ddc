<?php

namespace App\Webhooks;

use Exception;

use Illuminate\Http\Request;

use Spatie\WebhookClient\WebhookProcessor as SpatieWebhookProcessor;
use Spatie\WebhookClient\WebhookConfig;
use Spatie\WebhookClient\Models\WebhookCall;
use App\Webhooks\WebhookFailed;
use App\Webhooks\WebhookConfig as NewConfig;
use Illuminate\Support\Facades\Log;

class WebhookProcessor extends SpatieWebhookProcessor
{
    public function __construct(Request $request, WebhookConfig $config)
    {
        $this->request = $request;
        $this->config = new NewConfig($config);
    }

    protected function processWebhook(WebhookCall $webhookCall): void
    {
        try {
            if (is_callable($this->config->typeField)) {
                $type = ($this->config->typeField)($webhookCall);
            } else {
                $type = $webhookCall->payload[$this->config->typeField] ?? null;
            }

            if (empty($type)) {
                throw WebhookFailed::missingType($webhookCall);
            }

            $jobConfigKey = str_replace('.', '_', $type);
            $jobClass = $this->config->typeJobs[$jobConfigKey] ?? '';
            if ($jobClass == '')
            {
                throw WebhookFailed::jobClassNotSet($type, $webhookCall);
            }

            if (! class_exists($jobClass)) {
                throw WebhookFailed::jobClassDoesNotExist($jobClass, $webhookCall);
            }

            $webhookCall->clearException();
            dispatch(new $jobClass($webhookCall));

        } catch (Exception $exception) {
            $webhookCall->saveException($exception);

            throw $exception;
        }
    }

}

