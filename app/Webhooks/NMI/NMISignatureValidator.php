<?php

namespace App\Webhooks\NMI;

use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;
use Spatie\WebhookClient\WebhookConfig;
use Spatie\WebhookClient\Exceptions\WebhookFailed;
use Spatie\WebhookClient\SignatureValidator\SignatureValidator as BaseValidator;

class NMISignatureValidator implements BaseValidator
{
    public function isValid(Request $request, WebhookConfig $config): bool
    {
        $signatureHeader = $request->header($config->signatureHeaderName);

        $signingSecret = $config->signingSecret;

        if (empty($signingSecret)) {
            throw WebhookFailed::signingSecretNotSet();
        }

        if (preg_match('/t=(.*),s=(.*)/', $signatureHeader, $matches)) {
            $nonce = $matches[1];
            $signature = $matches[2];
        } else {
            throw WebhookFailed::invalidSignature();
        }

        $computedSignature = hash_hmac('sha256', $nonce . "." . $request->getContent(), $signingSecret);

        return hash_equals($signature, $computedSignature);
    }

}
