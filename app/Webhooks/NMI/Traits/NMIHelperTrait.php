<?php

namespace App\Webhooks\NMI\Traits;

use App\Subscription;

use Illuminate\Support\Facades\DB;

trait NMIHelperTrait
{
    protected function event()
    {
        return $this->webhookCall->payload['event_body'];
    }

    protected function isSubscriptionPayment()
    {
        return ($this->webhookCall->payload['event_body']['action']['source'] == 'recurring');
    }

    protected function billingEmail()
    {
        return $this->webhookCall->payload['event_body']['billing_address']['email'];
    }

    protected function amount()
    {
        return $this->webhookCall->payload['event_body']['requested_amount'];
    }

    protected function transactionDate()
    {
        return $this->webhookCall->created_at->format('Y-m-d');
    }

    protected function transactionDay()
    {
        return $this->webhookCall->created_at->day;
    }

    protected function getMatchingSubscription()
    {
        // if the same member creates more than one subscription on the same day for the same exact total, this will break.
        // solution: if there is more than one match, return the one with the oldest related transaction
        $day = $this->transactionDay();
        $subs = Subscription::where(DB::raw('amount + processing_fee + tip'),$this->amount())
                            ->whereHas('member', function($query) {
                                $query->where('email',$this->billingEmail());
                            })
                            ->where(function($query) use ($day) {
                                if ($day >= 28) {
                                    $query->whereIn( DB::raw("DAY(created_at)") , [27,28,29,30,31]);
                                } else {
                                    $query->whereIn( DB::raw("DAY(created_at)") , [$day-1,$day,$day+1]);
                                }
                            })
                            ->withMax('transactions','created_at')
                            ->get();

        if (!$subs || $subs->count() == 0) {
            return null;
        }

        return $subs->sortBy('transactions_max_created_at')->first();
    }

    // protected function getSubscriptionId()
    // {
    //     $event = $this->event();
    //     if (array_key_exists('subscription_id', $event)) {
    //         return $event['subscription_id'];
    //     }
    //     if (array_key_exists('subscription', $event) && is_array($event['subscription'])) {
    //         foreach (['id', 'subscription_id'] as $key) {
    //             if (array_key_exists($key,$event['subscription'])) {
    //                 return $event['subscription'][$key];
    //             }
    //         }
    //     }

    //     return null;
    // }
}
