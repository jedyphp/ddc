<?php

namespace App\Webhooks;

use \Spatie\WebhookClient\Exceptions\WebhookFailed as SpatieWebhookFailed;
use Spatie\WebhookClient\Models\WebhookCall;

class WebhookFailed extends SpatieWebhookFailed
{
    public static function missingType(): WebhookFailed
    {
        return new static('The webhook payload did not include the expected type field.');
    }

    public static function jobClassNotSet(string $jobClass, WebhookCall $webhookCall): WebhookFailed
    {
        return new static("Could not process webhook id `{$webhookCall->id}` of type `{$webhookCall->type}` because the specified type does not have a class configured.");
    }

    public static function jobClassDoesNotExist(string $jobClass, WebhookCall $webhookCall): WebhookFailed
    {
        return new static("Could not process webhook id `{$webhookCall->id}` of type `{$webhookCall->type}` because the configured jobclass `$jobClass` does not exist.");
    }
}
