<?php

namespace App\Webhooks\Braintree;

use Exception;

use Illuminate\Http\Request;
use Spatie\WebhookClient\WebhookConfig;
use Spatie\WebhookClient\Exceptions\WebhookFailed;
use App\Webhooks\Braintree\Traits\BraintreeGatewayTrait;
use Spatie\WebhookClient\SignatureValidator\SignatureValidator as BaseValidator;

class BraintreeSignatureValidator implements BaseValidator
{
    use BraintreeGatewayTrait;

    public function isValid(Request $request, WebhookConfig $config): bool
    {
        $signature = $request->input($config->signatureHeaderName);

        try {
            $webhookNotification = $this->parseBraintreeWebhookNotification($signature,$request->input('bt_payload'));
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}
