<?php

namespace App\Webhooks\Braintree;

use Exception;

use Illuminate\Http\Request;
use Spatie\WebhookClient\WebhookConfig;
use App\Webhooks\Braintree\Traits\BraintreeGatewayTrait;
use Spatie\WebhookClient\Models\WebhookCall as SpatieWebhookCall;

class BraintreeWebhookCall extends SpatieWebhookCall
{
    use BraintreeGatewayTrait;

    protected $table = 'webhook_calls';

    public function getDataAttribute()
    {
        return $this->parseBraintreeWebhookNotification($this->payload['bt_signature'], $this->payload['bt_payload']);
    }
}
