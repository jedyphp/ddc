<?php

namespace App\Webhooks\Braintree\Traits;

use Braintree\Gateway as BraintreeGateway;

trait BraintreeGatewayTrait
{
    protected function getBraintreeGateway() {
        return new BraintreeGateway([
            'environment' => config('braintree.environment'),
            'merchantId' => config('braintree.merchant_id'),
            'publicKey' => config('braintree.public_key'),
            'privateKey' => config('braintree.private_key')
        ]);
    }

    protected function parseBraintreeWebhookNotification($signature, $payload) {
        $gateway = $this->getBraintreeGateway();

        return $gateway->webhookNotification()->parse($signature, $payload);
    }
}
