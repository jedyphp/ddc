<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class MetricProject extends Pivot
{
    protected $hidden = ['metric_id','project_id','created_at','updated_at'];
    public $timestamps = false;

    public function getTotalFromDollars($dollars)
    {
        return $this->dollarValue * $dollars;
    }

    public function setTotalFromDollars($dollars)
    {
        $this->total = $this->getTotalFromDollars($dollars);
    }

}
