<?php

namespace App\Observers;

use App\Member;

class MemberObserver
{
    /**
     * Handle the Member "created" event.
     *
     * @param  \App\Member  $member
     * @return void
     */
    public function created(Member $member)
    {
        $member->wallets()->create([
            'member_id' => $member->id,
        ]);
    }
}
