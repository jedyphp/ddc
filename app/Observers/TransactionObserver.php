<?php

namespace App\Observers;

use App\Transaction;

class TransactionObserver
{
    /**
     * Handle the Transaction "created" event.
     *
     * @param  \App\Transaction  $transaction
     * @return void
     */
    public function created(Transaction $transaction)
    {
        $transaction->inflows()->create([
            'transaction_id' => $transaction->id,
            'wallet_id' => $transaction->member->wallets[0]->id,
            'amount' => $transaction->amount,
        ]);
    }
}
