<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metric extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function projects()
    {
        return $this->belongsToMany('App\Project')->using('App\MetricProject')->as('progress')->withPivot('total','target','dollarRatio','icon_url');
    }

    public function achievements()
    {
        return $this->hasMany('App\Achievement');
    }

    public function members()
    {
        return $this->belongsToMany('App\Member')->using('App\MemberMetric')->as('progress')->withPivot('total');
    }
}
