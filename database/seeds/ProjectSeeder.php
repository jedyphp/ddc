<?php

use Carbon\Carbon;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = [
            [
                'id' => 1,
                'parent_id' => null,
                '_lft' => 0,
                '_rgt' => 5,
                'status' => 'active',
                'title' => 'Plant 1 Trillion Trees',
                'description' => null,
                'outcomes_primary' => json_encode([
                    '2.4 million trees planted.',
                    '600 families out of hunger (continuously).',
                    '1 Forest Garden = 7.7 tons of organic produce by year 4!',
                    '600 families increase income by 400% on avg.',
                    '252,301 tons of C02 sequestered over 20 years (about 54,848 cars off the road in a year)',
                    'Restores +875 acres of land.'
                ]),
                'outcomes_secondary' => json_encode([
                    'Massively increases nutrition for 600 families.',
                    'Increases local animal & insect biodiversity.',
                    'Restores natural ecosystem (de-monocropping).',
                    'Reduces gender inequalities (+40% women).',
                    'Aids watershed restoration.',
                    'Improves local economy & opportunity.',
                    'Biodiverse crops far more resilient to failure.',
                    'Increased land resiliency to natural disaster.',
                    'Added income often leads to child-education.'
                ]),
                'sdg_numbers' => json_encode([1,2,3,5,8,10,12,13,15,17]),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'parent_id' => 1,
                '_lft' => 1,
                '_rgt' => 4,
                'status' => 'active',
                'title' => 'Create 1,000,000 Forest Gardens in Sub-Saharan Africa',
                'description' => null,
                'outcomes_primary' => null,
                'outcomes_secondary' => null,
                'sdg_numbers' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'parent_id' => 2,
                '_lft' => 2,
                '_rgt' => 3,
                'status' => 'active',
                'title' => 'Plant 2.4 million trees in Tanzania, eradicating hunger & poverty for 600 families',
                'description' => 'A group-quest on the Trillion Trees mega-goal',
                'outcomes_primary' => null,
                'outcomes_secondary' => null,
                'sdg_numbers' => null,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
            ];
        DB::table('projects')->insert($projects);

        $metrics = [
            [
                'id' => 1,
                'uid' => 'trees',
                'title' => 'tree',
                'titlePlural' => 'trees',
                'titlePhrase' => 'trees planted',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'uid' => 'co2',
                'title' => 'CO2',
                'titlePlural' => 'CO2',
                'titlePhrase' => 'lbs CO2 removed/yr',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'uid' => 'produce',
                'title' => 'organic produce',
                'titlePlural' => 'organic produce',
                'titlePhrase' => 'lbs organic produce',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 4,
                'uid' => 'land',
                'title' => 'land',
                'titlePlural' => 'land',
                'titlePhrase' => 'sq. ft. land restored',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 5,
                'uid' => 'forestGarden',
                'title' => 'forest garden',
                'titlePlural' => 'forest gardens',
                'titlePhrase' => 'forest gardens planted',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id' => 6,
                'uid' => 'families',
                'title' => 'family',
                'titlePlural' => 'families',
                'titlePhrase' => 'families with increased income',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];

        DB::table('metrics')->insert($metrics);

        $achievements = [
            [
                'id' => 1,
                'project_id' => 3,
                'metric_id' => 5,
                'title' => 'Planting 600 Forest Gardens',
                'target' => 600,
                'description' => '1 Forest Garden helps 1 family continuously out of hunger and plants 4,000 trees',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ];

        DB::table('achievements')->insert($achievements);

        $metric_project = [
            [
                'project_id' => 1,
                'metric_id' => 1,
                'total' => 0,
                'target' => 1000000000000,
                'dollarRatio' => null,
                'primary' => true,
                'sort_index' => 0
            ],
            [
                'project_id' => 1,
                'metric_id' => 2,
                'total' => 0,
                'target' => null,
                'dollarRatio' => null,
                'primary' => false,
                'sort_index' => 1
            ],
            [
                'project_id' => 1,
                'metric_id' => 3,
                'total' => 0,
                'target' => null,
                'dollarRatio' => 15,
                'primary' => false,
                'sort_index' => 2
            ],
            [
                'project_id' => 1,
                'metric_id' => 4,
                'total' => 0,
                'target' => null,
                'dollarRatio' => null,
                'primary' => false,
                'sort_index' => 3
            ],
            [
                'project_id' => 1,
                'metric_id' => 5,
                'total' => 0,
                'target' => null,
                'dollarRatio' => null,
                'primary' => false,
                'sort_index' => 4
            ],
            [
                'project_id' => 2,
                'metric_id' => 5,
                'total' => 0,
                'target' => 1000000,
                'dollarRatio' => null,
                'primary' => true,
                'sort_index' => 0
            ],
            [
                'project_id' => 2,
                'metric_id' => 4,
                'total' => 0,
                'target' => null,
                'dollarRatio' => null,
                'primary' => false,
                'sort_index' => 1
            ],
            [
                'project_id' => 2,
                'metric_id' => 2,
                'total' => 0,
                'target' => null,
                'dollarRatio' => null,
                'primary' => false,
                'sort_index' => 2
            ],
            [
                'project_id' => 2,
                'metric_id' => 6,
                'total' => 0,
                'target' => null,
                'dollarRatio' => null,
                'primary' => false,
                'sort_index' => 3
            ],
            [
                'project_id' => 2,
                'metric_id' => 1,
                'total' => 0,
                'target' => null,
                'dollarRatio' => null,
                'primary' => false,
                'sort_index' => 4
            ],
            [
                'project_id' => 3,
                'metric_id' => 1,
                'total' => 0,
                'target' => 2400000000,
                'dollarRatio' => 4,
                'primary' => true,
                'sort_index' => 0
            ],
            [
                'project_id' => 3,
                'metric_id' => 4,
                'total' => 0,
                'target' => null,
                'dollarRatio' => 63.56,
                'primary' => false,
                'sort_index' => 1
            ],
            [
                'project_id' => 3,
                'metric_id' => 2,
                'total' => 0,
                'target' => null,
                'dollarRatio' => 62,
                'primary' => false,
                'sort_index' => 2
            ],
            [
                'project_id' => 3,
                'metric_id' => 6,
                'total' => 0,
                'target' => null,
                'dollarRatio' => 0.001,
                'primary' => false,
                'sort_index' => 3
            ],
            [
                'project_id' => 3,
                'metric_id' => 5,
                'total' => 0,
                'target' => null,
                'dollarRatio' => 0.001,
                'primary' => false,
                'sort_index' => 4
            ],
        ];

        DB::table('metric_project')->insert($metric_project);

    }
}
