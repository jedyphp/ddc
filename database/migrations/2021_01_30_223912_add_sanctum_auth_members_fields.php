<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSanctumAuthMembersFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            //$table->id();
            $table->string('name')->after('referralsMonthly');
            $table->timestamp('email_verified_at')->nullable()->after('email');
            $table->string('password')->after('email');
            $table->json('permissions')->default('[]');
            $table->rememberToken();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            //
            $table->dropColumn('name');
            $table->dropColumn('email_verified_at');
            $table->dropColumn('password');
            $table->dropColumn('permissions');
            $table->dropRememberToken();
        });
    }
}
