<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AbstractPaymentProcessorFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('processor');
            $table->json('processor_data')->default('{}');
        });

        DB::table('subscriptions')->update(['processor' => "braintree",'processor_data' => DB::raw("JSON_SET(processor_data, '$.processor', 'braintree', '$.id', `braintree_subscription_id`)")]);

        Schema::table('transactions', function (Blueprint $table) {
            $table->string('processor');
            $table->json('processor_data')->default('{}');
        });

        DB::table('transactions')->update(['processor' => "braintree",'processor_data' => DB::raw("JSON_SET(processor_data, '$.processor', 'braintree', '$.id', `braintree_transaction_id`)")]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            //
            $table->dropColumn('processor');
            $table->dropColumn('processor_data');
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            //
            $table->dropColumn('processor');
            $table->dropColumn('processor_data');
        });
    }
}
