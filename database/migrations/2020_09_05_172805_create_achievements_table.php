<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievements', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('project_id')->index();
            $table->bigInteger('metric_id')->index();
            $table->string('title');
            $table->bigInteger('target')->unsigned();
            $table->text('description');
            $table->string('image_url')->nullable();
            $table->boolean('achieved')->default(false)->index();
            $table->index(['project_id','metric_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievements');
    }
}
