<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('member_id')->index();
            $table->string('braintree_subscription_id',36)->unique();
            $table->enum('status',['active','inactive','past_due'])->default('inactive');
            $table->string('plan',16);
            $table->decimal('amount',9,2);
            $table->decimal('processing_fee',9,2);
            $table->decimal('tip',9,2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
