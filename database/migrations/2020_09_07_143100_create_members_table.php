<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->nestedSet();
            $table->enum('status',['pending','active','inactive','past_due'])->default('pending');
            $table->string('email')->unique()->index();
            $table->float('total_donations')->default(0.0);
            $table->float('total_tips')->default(0.0);
            $table->float('points',12,4)->unsigned()->default(0)->index();
            $table->float('pointsMonthly',12,4)->unsigned()->default(0);
            $table->integer('referrals')->unsigned()->default(0)->index();
            $table->integer('referralsMonthly')->unsigned()->default(0);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('street')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('postal_code')->nullable();
            $table->string('country');
            $table->string('referral_code')->unique()->nullable()->index();
            $table->bigInteger('project_id')->nullable();
        });

        Schema::create('member_metric', function (Blueprint $table) {
            $table->bigInteger('member_id')->index();
            $table->bigInteger('metric_id');
            $table->float('total',12,4)->unsigned()->default(0);
            $table->float('totalMonthly',12,4)->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
        Schema::dropIfExists('member_metric');
    }
}
