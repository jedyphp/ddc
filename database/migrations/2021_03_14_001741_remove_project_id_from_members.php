<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Partner;

class RemoveProjectIdFromMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('partners', function (Blueprint $table) {
            $table->bigInteger('project_id')->after('member_id')->nullable();
        });

        Partner::with('member')->get()->each(function($partner) {
            $partner->project_id = $partner->member->project_id;
            $partner->save();
        });

        Schema::table('members', function (Blueprint $table) {
            $table->dropColumn('project_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->bigInteger('project_id')->nullable();
        });

        Partner::with('member')->get()->each(function($partner) {
            $partner->member->project_id = $partner->project_id;
            $partner->save();
        });

        Schema::table('partners', function (Blueprint $table) {
            $table->dropColumn('project_id');
        });
    }
}
