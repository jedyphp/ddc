<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->nestedSet();
            $table->enum('status',['pending','active','complete','inactive'])->default('pending')->index();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('photo_url')->nullable();
            $table->bigInteger('dollars')->default(0);
            $table->bigInteger('dollarsMonthly')->default(0);
            $table->json('outcomes_primary')->nullable();
            $table->json('outcomes_secondary')->nullable();
            $table->json('sdg_numbers')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
