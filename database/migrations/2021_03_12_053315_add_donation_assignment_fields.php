<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDonationAssignmentFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_transaction', function (Blueprint $table) {
            $table->bigInteger('project_id')->index();
            $table->bigInteger('transaction_id')->index();
            $table->index(['project_id','transaction_id']);
            $table->float('amount',12,4)->unsigned()->default(0);
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->json('allocation')->default('{}');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->json('allocation')->default('{}');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('allocation');
        });

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn('allocation');
        });

        Schema::dropIfExists('project_transaction');
    }
}
