<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metrics', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('uid',32);
            $table->string('title');
            $table->string('titlePlural');
            $table->string('titlePhrase');
            $table->string('icon_url_default')->nullable();
        });

        Schema::create('metric_project', function (Blueprint $table) {
            $table->bigInteger('project_id')->index();
            $table->bigInteger('metric_id');
            $table->float('total',12,4)->unsigned()->default(0);
            $table->float('totalMonthly',12,4)->unsigned()->default(0);
            $table->bigInteger('target')->unsigned()->nullable();
            $table->float('dollarRatio',12,4)->unsigned()->nullable();
            $table->string('icon_url')->nullable();
            $table->boolean('primary')->default(false);
            $table->integer('sort_index')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metrics');
        Schema::dropIfExists('metric_project');
    }
}
