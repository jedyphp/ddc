<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CleanupPaymentProcessorIdFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function (Blueprint $table) {
            $table->string('subscription_id')->nullable();
            $table->unique(['processor','subscription_id'],'unique_processor_subscription_id_index');
        });

        DB::table('subscriptions')
            ->update(['subscription_id' => DB::raw("IF(LOCATE(':',braintree_subscription_id)>0,SUBSTRING_INDEX(braintree_subscription_id,':',-1),braintree_subscription_id)"),
                      'processor' => DB::raw("IF(LOCATE(':',braintree_subscription_id)>0,SUBSTRING_INDEX(braintree_subscription_id,':',1),'braintree')")]);

        Schema::table('transactions', function (Blueprint $table) {
            $table->string('transaction_id')->nullable();
            $table->unique(['processor','transaction_id'],'unique_processor_transaction_id_index');
        });

        DB::table('transactions')
            ->update(['transaction_id' => DB::raw("IF(LOCATE(':',braintree_transaction_id)>0,SUBSTRING_INDEX(braintree_transaction_id,':',-1),braintree_transaction_id)"),
                      'processor' => DB::raw("IF(LOCATE(':',braintree_transaction_id)>0,SUBSTRING_INDEX(braintree_transaction_id,':',1),'braintree')")]);

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropColumn('braintree_subscription_id');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->dropColumn('braintree_transaction_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('subscriptions')->update(['subscription_id' => DB::raw("CONCAT(processor,':',subscription_id)")]);
        DB::table('transactions')->update(['transaction_id' => DB::raw("CONCAT(processor,':',transaction_id)")]);

        Schema::table('subscriptions', function (Blueprint $table) {
            $table->dropIndex('unique_processor_subscription_id_index');
            $table->dropColumn('subscription_id');
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->dropIndex('unique_processor_transaction_id_index');
            $table->dropColumn('transaction_id');
        });
    }
}
