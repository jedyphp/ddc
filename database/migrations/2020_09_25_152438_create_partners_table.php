<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('member_id')->index();
            $table->string('name')->nullable();
            $table->string('photo_path')->nullable();
            $table->longText('description')->nullable();
            $table->float('impactAcceleratorTotal')->default(0.0);
            $table->float('impactAcceleratorUsed')->default(0.0);
            $table->float('impactAcceleratorMonthly')->default(0.0);
            $table->json('donation_prizes')->nullable();
            $table->json('donation_direct')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
