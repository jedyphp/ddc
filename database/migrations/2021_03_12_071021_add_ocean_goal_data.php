<?php

use App\Project;
use App\Metric;

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class AddOceanGoalData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $goal = new Project;
        $goal->title = "Rid the world's oceans of plastic waste";
        $goal->status = 'active';
        $goal->save();

        $metric = new Metric;
        $metric->uid = 'oceanPlastic';
        $metric->title = 'plastic';
        $metric->titlePlural = 'plastic';
        $metric->titlePhrase = 'lbs. plastic waste removed from the ocean';

        $goal->metrics()->save($metric,[
            'dollarRatio' => 0.33,
            'primary' => 1,
            'sort_index' => 0
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $metric = Metric::where('uid','=','oceanPlastic')->first();
        $metric->projects->each(function($project) use ($metric) {
            $metric->projects()->detach($project->id);
            $project->delete();
        });
        $metric->delete();
    }
}
