WHEN A PARTNER JOINS:
1. A member record, along with associated subscription, transaction, and partner records, is created.
2. The referring member, if any, gains an impact point bounty based on the following table (in the format "dollar amount" => "bounty"):
        '100' => 200,
        '200' => 400,
        '400' => 800,
        '700' => 1400,
        '1000' => 2000,
        '2000' => 3000,
        '5000' => 5000,
        '10000' => 10000,
        '50000' => 20000,
        '100000' => 35000,
        '500000' => 60000,
        '1000000' => 80000

    Whichever the highest dollar amount that is is equal to or less than the partner contribution is the row selected and points distributed.

3. A subscription record and transaction record are created for the partner.
4. The created transaction record is passed into the transaction processing queue.

WHEN A MEMBER JOINS:
1. A member record is created, along with associated subscription and transaction records.
2. The created transaction record is passed into the transaction processing queue.
3. Accelerate the impact of the member joining by moving $1 from each partner with impact accelerator remaining to that partners designated project.

WHEN BRAINTREE SENDS A PAYLOAD:
1. The subscription record is determined by the braintree subscription ID.
2. A new transaction for the subscription is created and passed into the transaction processing queue.

TRANSACTION PROCESSING QUEUE:
1. Determine if the transaction is associated with a MEMBER or PARTNER, and process the determined transaction type:

TRANSACTION PROCESSING: PARTNER:
1. 
