# DDC API v1 Preliminary Documentation

**All endpoints are located at https://api.dollardonationclub.com/v1**

## Members:

### ***GET:* /members/{member_id}**
Retrieve the member specified by member_id

**Parameters:**
- "member_id"
  - location: path
  - format: integer

**Responses**
- "200": Success! Returns Member Object
- "404": Member not found for the provided member_id

### ***PUT:* /members/{member_id}**
Update the member specified by member_id

**Parameters:**
- "member_id"
  - location: path
  - format: integer
- "first_name"
  - location: request
  - format: string, max 255 chars
  - **required**
- "last_name"
  - location: request
  - format: string, max 255 chars
  - **required**
- "email"
  - location: request
  - format: email address, unique
  - **required**
- "street"
  - location: request
  - format: string, max 255 chars
  - **required**
- "city"
  - location: request
  - format: string, max 255 chars
  - **required**
- "state"
  - location: request
  - format: string, max 255 chars
  - **required**
- "postal_code"
  - location: request
  - format: string, max 255 chars
  - **required**
- "country"
  - location: request
  - format: string, max 255 chars
  - **required**

**Responses**
- **200**: Success! Returns Member Object
- **404**: Member not found for the provided member_id
- **422**: Validation Error! Returns JSON object with array of error messages by field.


### ***PUT:* /members**
Create a new member

**Parameters:**
- "first_name"
  - location: request
  - format: string, max 255 chars
  - **required**
- "last_name"
  - location: request
  - format: string, max 255 chars
  - **required**
- "email"
  - location: request
  - format: email address, unique
  - **required**
- "street"
  - location: request
  - format: string, max 255 chars
  - **required**
- "city"
  - location: request
  - format: string, max 255 chars
  - **required**
- "state"
  - location: request
  - format: string, max 255 chars
  - **required**
- "postal_code"
  - location: request
  - format: string, max 255 chars
  - **required**
- "country"
  - location: request
  - format: string, max 255 chars
  - **required**
- "braintree_subscription_id"
  - location: request
  - format: string, max 255 chars, unique
  - description: The braintree unique ID for the member's subscription.
  - **required**
- "braintree_subscription_plan"
  - location: request
  - format: string, max 16 chars
  - description: The ID string for the chosen subscription plan.
  - **required**
- "braintree_subscription_amount"
  - location: request
  - format: decimal,9,2
  - description: The subscription amount
  - **required**
- "braintree_subscription_id"
  - location: request
  - format: decimal,9,2
  - description: The tip amount (default 0.0)

- "referrer_code"
  - location: request
  - format: string, max 255 chars, must be a valid referral code
  - description: The referral code provided by the member. The member will be created as a child of the member matching the referrer_code.

**Responses**
- **200**: Success! Returns Member Object
- **422**: Validation Error! Returns JSON object with array of error messages by field.

---
***Returned Member Example Object:***
```
{
    "id": 8,
    "created_at": "2020-09-10T23:37:03.000000Z",
    "updated_at": "2020-09-11T21:20:04.000000Z",
    "parent_id": 7,
    "status": "pending",
    "email": "rabrams@r8.com",
    "total_donations": 2,
    "points": 2,
    "pointsMonthly": 2,
    "referrals": 0,
    "referralsMonthly": 0,
    "first_name": "Ryan",
    "last_name": "Abrams",
    "street": "12345 Main St",
    "city": "Leonard",
    "state": "TX",
    "postal_code": "75452",
    "country": "USA",
    "referral_code": "EN6C6V",
    "project_id": 3,
    "ranks": "{\"city\":1,\"state\":1,\"country\":1,\"global\":1}",
    "metrics": [
        {
            "id": 3,
            "uid": "produce",
            "title": "organic produce",
            "titlePlural": "organic produce",
            "titlePhrase": "lbs organic produce",
            "icon_url_default": null,
            "progress": {
                "total": 30
            }
        },
        {
            "id": 1,
            "uid": "trees",
            "title": "tree",
            "titlePlural": "trees",
            "titlePhrase": "trees planted",
            "icon_url_default": null,
            "progress": {
                "total": 8
            }
        },
        {
            "id": 4,
            "uid": "land",
            "title": "land",
            "titlePlural": "land",
            "titlePhrase": "sq. ft. land restored",
            "icon_url_default": null,
            "progress": {
                "total": 127.12
            }
        },
        {
            "id": 2,
            "uid": "co2",
            "title": "CO2",
            "titlePlural": "CO2",
            "titlePhrase": "lbs CO2 removed/yr",
            "icon_url_default": null,
            "progress": {
                "total": 124
            }
        },
        {
            "id": 6,
            "uid": "families",
            "title": "family",
            "titlePlural": "families",
            "titlePhrase": "families with increased income",
            "icon_url_default": null,
            "progress": {
                "total": 0.002
            }
        },
        {
            "id": 5,
            "uid": "forestGarden",
            "title": "forest garden",
            "titlePlural": "forest gardens",
            "titlePhrase": "forest gardens planted",
            "icon_url_default": null,
            "progress": {
                "total": 0.002
            }
        }
    ]
}
```
---

## Goals:

*Note: Goals are referred to as projects internally*

### ***GET:* /goals/{project_id}**
Retrieve the goal specified by project_id

**Parameters:**
- "project_id"
  - location: path
  - format: integer
  - **required**

**Responses**
- "200": Success! Returns Project Object with child projects
- "404": Goal not found for the provided project_id

### ***GET:* /goals**
Retrive all goals, in heirarchical object form

**Responses**
- "200": Success! Returns JSON Array of all Root Projects, with child projects
- "404": Goal not found for the provided project_id

---
***Returned Projects Example Array:***
```
[
    {
        "id": 1,
        "parent_id": null,
        "status": "active",
        "title": "Plant 1 Trillion Trees",
        "description": null,
        "photo_url": null,
        "dollars": 2,
        "dollarsMonthly": 2,
        "outcomes_primary": [
            "2.4 million trees planted.",
            "600 families out of hunger (continuously).",
            "1 Forest Garden = 7.7 tons of organic produce by year 4!",
            "600 families increase income by 400% on avg.",
            "252,301 tons of C02 sequestered over 20 years (about 54,848 cars off the road in a year)",
            "Restores +875 acres of land."
        ],
        "outcomes_secondary": [
            "Massively increases nutrition for 600 families.",
            "Increases local animal & insect biodiversity.",
            "Restores natural ecosystem (de-monocropping).",
            "Reduces gender inequalities (+40% women).",
            "Aids watershed restoration.",
            "Improves local economy & opportunity.",
            "Biodiverse crops far more resilient to failure.",
            "Increased land resiliency to natural disaster.",
            "Added income often leads to child-education."
        ],
        "sdg_numbers": [
            1,
            3,
            5,
            9,
            10,
            15
        ],
        "metrics": [
            {
                "id": 1,
                "uid": "trees",
                "title": "tree",
                "titlePlural": "trees",
                "titlePhrase": "trees planted",
                "icon_url_default": null,
                "progress": {
                    "total": 8,
                    "target": 1000000000000,
                    "dollarRatio": null,
                    "icon_url": null,
                    "primary": 1,
                    "sort_index": 0
                }
            },
            {
                "id": 2,
                "uid": "co2",
                "title": "CO2",
                "titlePlural": "CO2",
                "titlePhrase": "lbs CO2 removed/yr",
                "icon_url_default": null,
                "progress": {
                    "total": 124,
                    "target": null,
                    "dollarRatio": null,
                    "icon_url": null,
                    "primary": 0,
                    "sort_index": 1
                }
            },
            {
                "id": 3,
                "uid": "produce",
                "title": "organic produce",
                "titlePlural": "organic produce",
                "titlePhrase": "lbs organic produce",
                "icon_url_default": null,
                "progress": {
                    "total": 30,
                    "target": null,
                    "dollarRatio": 15,
                    "icon_url": null,
                    "primary": 0,
                    "sort_index": 2
                }
            },
            {
                "id": 4,
                "uid": "land",
                "title": "land",
                "titlePlural": "land",
                "titlePhrase": "sq. ft. land restored",
                "icon_url_default": null,
                "progress": {
                    "total": 127.12,
                    "target": null,
                    "dollarRatio": null,
                    "icon_url": null,
                    "primary": 0,
                    "sort_index": 3
                }
            },
            {
                "id": 5,
                "uid": "forestGarden",
                "title": "forest garden",
                "titlePlural": "forest gardens",
                "titlePhrase": "forest gardens planted",
                "icon_url_default": null,
                "progress": {
                    "total": 0.002,
                    "target": null,
                    "dollarRatio": null,
                    "icon_url": null,
                    "primary": 0,
                    "sort_index": 4
                }
            }
        ],
        "achievements": [],
        "children": [
            {
                "id": 2,
                "parent_id": 1,
                "status": "active",
                "title": "Create 1,000,000 Forest Gardens in Sub-Saharan Africa",
                "description": null,
                "photo_url": null,
                "dollars": 2,
                "dollarsMonthly": 2,
                "outcomes_primary": null,
                "outcomes_secondary": null,
                "sdg_numbers": null,
                "metrics": [
                    {
                        "id": 5,
                        "uid": "forestGarden",
                        "title": "forest garden",
                        "titlePlural": "forest gardens",
                        "titlePhrase": "forest gardens planted",
                        "icon_url_default": null,
                        "progress": {
                            "total": 0.002,
                            "target": 1000000,
                            "dollarRatio": null,
                            "icon_url": null,
                            "primary": 1,
                            "sort_index": 0
                        }
                    },
                    {
                        "id": 4,
                        "uid": "land",
                        "title": "land",
                        "titlePlural": "land",
                        "titlePhrase": "sq. ft. land restored",
                        "icon_url_default": null,
                        "progress": {
                            "total": 127.12,
                            "target": null,
                            "dollarRatio": null,
                            "icon_url": null,
                            "primary": 0,
                            "sort_index": 1
                        }
                    },
                    {
                        "id": 2,
                        "uid": "co2",
                        "title": "CO2",
                        "titlePlural": "CO2",
                        "titlePhrase": "lbs CO2 removed/yr",
                        "icon_url_default": null,
                        "progress": {
                            "total": 124,
                            "target": null,
                            "dollarRatio": null,
                            "icon_url": null,
                            "primary": 0,
                            "sort_index": 2
                        }
                    },
                    {
                        "id": 6,
                        "uid": "families",
                        "title": "family",
                        "titlePlural": "families",
                        "titlePhrase": "families with increased income",
                        "icon_url_default": null,
                        "progress": {
                            "total": 0.002,
                            "target": null,
                            "dollarRatio": null,
                            "icon_url": null,
                            "primary": 0,
                            "sort_index": 3
                        }
                    },
                    {
                        "id": 1,
                        "uid": "trees",
                        "title": "tree",
                        "titlePlural": "trees",
                        "titlePhrase": "trees planted",
                        "icon_url_default": null,
                        "progress": {
                            "total": 8,
                            "target": null,
                            "dollarRatio": null,
                            "icon_url": null,
                            "primary": 0,
                            "sort_index": 4
                        }
                    }
                ],
                "achievements": [],
                "children": [
                    {
                        "id": 3,
                        "parent_id": 2,
                        "status": "active",
                        "title": "Plant 2.4 million trees in Singida, eradicating hunger & poverty for 600 families",
                        "description": "A group-quest on the Trillion Trees mega-goal",
                        "photo_url": null,
                        "dollars": 2,
                        "dollarsMonthly": 2,
                        "outcomes_primary": null,
                        "outcomes_secondary": null,
                        "sdg_numbers": null,
                        "metrics": [
                            {
                                "id": 1,
                                "uid": "trees",
                                "title": "tree",
                                "titlePlural": "trees",
                                "titlePhrase": "trees planted",
                                "icon_url_default": null,
                                "progress": {
                                    "total": 8,
                                    "target": 2400000000,
                                    "dollarRatio": 4,
                                    "icon_url": null,
                                    "primary": 1,
                                    "sort_index": 0
                                }
                            },
                            {
                                "id": 4,
                                "uid": "land",
                                "title": "land",
                                "titlePlural": "land",
                                "titlePhrase": "sq. ft. land restored",
                                "icon_url_default": null,
                                "progress": {
                                    "total": 127.12,
                                    "target": null,
                                    "dollarRatio": 63.56,
                                    "icon_url": null,
                                    "primary": 0,
                                    "sort_index": 1
                                }
                            },
                            {
                                "id": 2,
                                "uid": "co2",
                                "title": "CO2",
                                "titlePlural": "CO2",
                                "titlePhrase": "lbs CO2 removed/yr",
                                "icon_url_default": null,
                                "progress": {
                                    "total": 124,
                                    "target": null,
                                    "dollarRatio": 62,
                                    "icon_url": null,
                                    "primary": 0,
                                    "sort_index": 2
                                }
                            },
                            {
                                "id": 6,
                                "uid": "families",
                                "title": "family",
                                "titlePlural": "families",
                                "titlePhrase": "families with increased income",
                                "icon_url_default": null,
                                "progress": {
                                    "total": 0.002,
                                    "target": null,
                                    "dollarRatio": 0.001,
                                    "icon_url": null,
                                    "primary": 0,
                                    "sort_index": 3
                                }
                            },
                            {
                                "id": 5,
                                "uid": "forestGarden",
                                "title": "forest garden",
                                "titlePlural": "forest gardens",
                                "titlePhrase": "forest gardens planted",
                                "icon_url_default": null,
                                "progress": {
                                    "total": 0.002,
                                    "target": null,
                                    "dollarRatio": 0.001,
                                    "icon_url": null,
                                    "primary": 0,
                                    "sort_index": 4
                                }
                            }
                        ],
                        "achievements": [
                            {
                                "id": 1,
                                "project_id": 3,
                                "metric_id": 5,
                                "title": "Planting 600 Forest Gardens",
                                "target": 600,
                                "description": "1 Forest Garden helps 1 family continuously out of hunger and plants 4,000 trees",
                                "image_url": null,
                                "achieved": 0
                            }
                        ],
                        "children": []
                    }
                ]
            }
        ]
    }
]
```
---
## Leaderboards:


### ***GET:* /leaderboard/{member_id}/{scope}**
Retrive the leaderboard for the current members location based on specified scope

### ***GET:* /leaderboard/monthly/{member_id}/{scope}**
Retrive the monthly leaderboard for the current members location based on specified scope

**Both routes above share the same parameters and responses**

**Parameters:**
- "member_id"
  - location: path
  - format: integer
  - **required**
- "scope"
  - location: path
  - format: enum('global','country','city','state')
  - default: global

**Responses**
- "200": Success! Returns Leaderboard Object

### ***GET:* /leaderboard***
Retrieve the global leaderboard for points.

### ***GET:* /leaderboard/monthly***
Retrieve the global leaderboard for points this month.

---
**Leaderboard Example Object**
```
[
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 2,
        "pointsMonthly": 2,
        "created_at": "2020-09-10 23:37:03",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 1
    },
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 1.8,
        "pointsMonthly": 1.8,
        "created_at": "2020-09-10 23:36:56",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 2
    },
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 1.6,
        "pointsMonthly": 1.6,
        "created_at": "2020-09-10 23:36:47",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 3
    },
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 1.2,
        "pointsMonthly": 1.2,
        "created_at": "2020-09-10 23:36:40",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 4
    },
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 1,
        "pointsMonthly": 1,
        "created_at": "2020-09-10 23:36:33",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 5
    },
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 0.6,
        "pointsMonthly": 0.6,
        "created_at": "2020-09-10 23:36:27",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 6
    },
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 0.4,
        "pointsMonthly": 0.4,
        "created_at": "2020-09-10 23:36:20",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 7
    },
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 0.2,
        "pointsMonthly": 0.2,
        "created_at": "2020-09-10 22:18:22",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 8
    },
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 0,
        "pointsMonthly": 0,
        "created_at": "2020-09-11 21:21:12",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 9
    },
    {
        "first_name": "Ryan",
        "last_name": "Abrams",
        "points": 0,
        "pointsMonthly": 0,
        "created_at": "2020-09-12 02:58:39",
        "city": "Leonard",
        "state": "TX",
        "country": "USA",
        "rank": 10
    }
]
```
