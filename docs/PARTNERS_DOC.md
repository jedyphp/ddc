# PARTNERS CHANGES

## Member Creation

### ***PUT:* /members**

Partners can be created along side their owning member. To do so, use the below parameters and information.

Please note: Members will include a partner sub object if relevant, which will include all partner information.

**Additional Parameters:**
- "type"
  - location: request
  - enum: 'member','partner'
  - default: 'member'

- "partner_name": This is a name for the partner itself, rather than the member
  - location: request
  - string
  - optional

- "partner_description": This is the paragraph of descriptive text.
  - location: request
  - long text
  - optional

**Notes on Existing Parameters**
- braintree_subscription_id = Unique ID for subscription
- braintree_subscription_amount = AI donation amount
- braintree_subscription_processingfee = 7% platform fee
- braintree_subscription_tip is not required

## Partner Maintenance

### ***POST:* /partners/{partner}/photo**
Upload a photo for the designated partner_id.

**Parameters:**
- "photo"
  - location: request
  - type: file
  
### ***PUT:* /partners/{partner}**

Update an existing partner by Partner ID. Partner ID is available in the sub-record of the partner's member object.

**Parameters:**
- "name": The partner name
  - location: request
  - optional
  - string
- "description": The long text description / paragraph
  - location: request
  - optional
  - long text
- "donation_prizes": A JSON storing the prizes. 
  - location: request
  - optional
  - format: json
    - This content can be whatever you would like, and is stored as sent from the front end admin.
- "donation_direct": A JSON of direct donation
  - location: request
  - optional
  - format: json
    - This content can be whatever you would like, and is stored as sent from the front end admin.
- "photo": update the partner photo (not the same as avatar)
  - location: request
  - optional
  - format: file (jpg, png, gif)
  - NOTE: To use this field, you must send the request to this endpoint as via POST, and include the **_method** parameter in the request, with the value of "put". Using this field via the PUT method, or without the _method parameter will generate an error.

### ***GET:* /partners/{partner}**

Retrive a specified partner by ID. The partner will include the associated member as a sub-object. Member metrics apply to partner.


### ***GET:* /partners**

Retrieve a list of ALL partners by ID. The partners will include the associated members as sub-objects. Member metrics apply to partner.


## Leaderboards

All leaderboards can be accessed scoped to partners at /leaderboard/partners, followed by the same endpoint request structure.

Please note that in these URLs, the member_id is passed for {member}, despite asking for partner leaderboards.

- /leaderboard/partners/monthly/{member}/{scope?}
- /leaderboard/partners/monthly
- /leaderboard/partners/{member}/{scope?}
- /leaderboard/partners/
