Route security requirements:

1. All POST and PUT routes for members and partners require that the associated member be the currently authenticated user, by JWT token.

2. The following are "admin" routes have been added. They do not require the JWT associate with the member being modified, but do require that the JWT token provided has 'admin.partners' in scope.

* POST: admin/members/{member}/avatar
* PUT: admin/members/bySub/{member:sub}
* PUT: admin/members/{member}
* POST: admin/partners/{partner}/photo
* PUT: admin/partners/{partner}

3. "admin/partners/{partner}" supports the parameters 'donation_prizes' and 'donation_direct' which are json fields. These can no longer be edited via the normal partner update route, and must be managed by admin.

4. The following routes are now enabled, and require the JWT token to have the scope 'admin.updates'

* POST: goals/{project}/updates
* PUT: updates/{update}
* DELETE: updates/{update}

