# Cache Tag Explanation for DDC API

## Tags

*p:<project_id>*: related to the specified project
*p:all*: Involves all projects. Gets cleared whenever a project changes.

*m:<member_id>* : related to the specified member
*m:all* : Involves all members. Cleared whenever a member changes.

*leaderboard* : this is a leaderboard of some type
