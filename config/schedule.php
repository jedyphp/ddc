<?php

return [
    'zeroMonthly' => [
        'projects' => 'dollarsMonthly',
        'metric_project' => 'totalMonthly',
        'member_metric' => 'totalMonthly',
        'members' => ['pointsMonthly','referralsMonthly'],
        'partners' => ['impactAcceleratorMonthly'],
    ],
    'nullMonthly' => [

    ]
];
