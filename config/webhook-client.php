<?php

return [
    'configs' => [
        [
            'name' => 'nmi-payments',
            'signing_secret' => env('NMI_WEBHOOK_CLIENT_SECRET'),
            'signature_header_name' => 'Webhook-Signature',
            'signature_validator' => \App\Webhooks\NMI\NMISignatureValidator::class,
            'webhook_profile' => \Spatie\WebhookClient\WebhookProfile\ProcessEverythingWebhookProfile::class,
            'webhook_response' => \Spatie\WebhookClient\WebhookResponse\DefaultRespondsTo::class,
            'webhook_model' => \Spatie\WebhookClient\Models\WebhookCall::class,
            'webhook_processor' => \App\Webhooks\WebhookProcessor::class,
            'process_webhook_job' => \App\Webhooks\WebhookJobBypass::class,
            'type_field' => 'event_type',
            'type_jobs' => [
                'transaction_sale_success' => \App\Jobs\NMI\TransactionSaleSuccess::class,
                'transaction_sale_failure' => \App\Jobs\NMI\TransactionSaleFailure::class,
            ]
        ],
        [
            'name' => 'paypal-payments',
            'signing_secret' => config('payments.paypal.webhook_id'),
            'signature_header_name' => 'not-relevant-to-paypal',
            'signature_validator' => \App\Webhooks\Paypal\PaypalSignatureValidator::class,
            'webhook_profile' => \Spatie\WebhookClient\WebhookProfile\ProcessEverythingWebhookProfile::class,
            'webhook_response' => \Spatie\WebhookClient\WebhookResponse\DefaultRespondsTo::class,
            'webhook_model' => \Spatie\WebhookClient\Models\WebhookCall::class,
            'webhook_processor' => \App\Webhooks\WebhookProcessor::class,
            'process_webhook_job' => \App\Webhooks\WebhookJobBypass::class,
            'type_field' => 'event_type',
            'type_jobs' => [
                'PAYMENT.SALE.COMPLETED' => \App\Jobs\Paypal\PaymentSaleCompleted::class,
                'BILLING.SUBSCRIPTION.PAYMENT.FAILED' => \App\Jobs\Paypal\SubscriptionPaymentFailed::class,
                'BILLING.SUBSCRIPTION.CANCELLED' => \App\Jobs\Paypal\SubscriptionCancelled::class,
                'BILLING.SUBSCRIPTION.SUSPENDED' => \App\Jobs\Paypal\SubscriptionSuspended::class,
                'PAYMENT_SALE_COMPLETED' => \App\Jobs\Paypal\PaymentSaleCompleted::class,
                'BILLING_SUBSCRIPTION_PAYMENT_FAILED' => \App\Jobs\Paypal\SubscriptionPaymentFailed::class,
                'BILLING_SUBSCRIPTION_CANCELLED' => \App\Jobs\Paypal\SubscriptionCancelled::class,
                'BILLING_SUBSCRIPTION_SUSPENDED' => \App\Jobs\Paypal\SubscriptionSuspended::class,
            ]
        ],
        [
            'name' => 'braintree-payments',
            // 'signing_secret' => env('BRAINTREE_WEBHOOK_CLIENT_SECRET'),
            'signature_header_name' => 'bt_signature',
            'signature_validator' => \App\Webhooks\Braintree\BraintreeSignatureValidator::class,
            'webhook_profile' => \Spatie\WebhookClient\WebhookProfile\ProcessEverythingWebhookProfile::class,
            'webhook_response' => \Spatie\WebhookClient\WebhookResponse\DefaultRespondsTo::class,
            'webhook_model' => \App\Webhooks\Braintree\BraintreeWebhookCall::class,
            'webhook_processor' => \App\Webhooks\WebhookProcessor::class,
            'process_webhook_job' => \App\Webhooks\WebhookJobBypass::class,
            'type_field' => function($webhookCall) { return $webhookCall->data->kind ?? null; },
            'type_jobs' => [
                \Braintree\WebhookNotification::SUBSCRIPTION_CHARGED_SUCCESSFULLY => \App\Jobs\Braintree\SubscriptionChargedSuccessfully::class,
                \Braintree\WebhookNotification::SUBSCRIPTION_CANCELED => \App\Jobs\Braintree\SubscriptionCancelled::class,
                \Braintree\WebhookNotification::SUBSCRIPTION_WENT_PAST_DUE => \App\Jobs\Braintree\SubscriptionWentPastDue::class,
                \Braintree\WebhookNotification::SUBSCRIPTION_WENT_ACTIVE => \App\Jobs\Braintree\SubscriptionWentActive::class
            ]
        ],

    ],
];
