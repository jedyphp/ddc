<?php

return [
    'processors' => [
        'nmi',
        'paypal',
        'cliq',
        'braintree'
    ],
    'paypal' => [
        'client_id' => env('PAYPAL_CLIENT_ID'),
        'secret' => env('PAYPAL_CLIENT_SECRET'),
        'webhook_id' => env('PAYPAL_WEBHOOK_ID'),
        'api_base' => env('PAYPAL_API_ROOT'),
    ],
    'receipt' => [
        'minimumTotal' => 20,
        'bcc' => ['flyingpigdonations@legacyglobal.org'],
        'organization' => 'The Flying Pig (Legacy Global)',
    ]
];
