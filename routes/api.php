<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ReportsController;
use App\Http\Controllers\WebhookController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\MembersController;
use App\Http\Controllers\Api\PartnersController;
use App\Http\Controllers\Api\LeaderboardController;
use App\Http\Controllers\Api\ProjectsController;
use App\Http\Controllers\Api\SubscriptionsController;
use App\Http\Controllers\Api\TransactionsController;
use App\Http\Controllers\Api\UpdatesController;
use App\Http\Controllers\Api\BraintreeController as PaymentsController;
use App\Http\Controllers\Api\BraintreeTestController as PaymentsTestController;
use App\Http\Controllers\Api\MembersVanityController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('reports')->group(function() {
    Route::get('transactions', [ReportsController::class,'transactionLog']);
    Route::get('wintogether', [ReportsController::class, 'wintogetherReferredAccounts']);
});

Route::prefix('webhook')->group(function() {
    // WebhookController does not require the namespace? (name is required for config matching in library)
    Route::post('braintree-payment', 'WebhookController')->name('webhook-client-braintree-payments');
    Route::post('nmi-payment', 'WebhookController')->name('webhook-client-nmi-payments');
    Route::post('paypal-payment', 'WebhookController')->name('webhook-client-paypal-payments');
});

// Dupe for now, in case old braintree happens.
Route::prefix('v1')->group(function() {
    Route::post('payment', 'WebhookController')->name('webhook-client-braintree-payments');
});

Route::namespace('api')->prefix('v1')->group(function() {

    // Routes for Administrators
    Route::prefix('admin')->middleware('auth:santum')->group(function() {
        Route::post('members/{member}/avatar', [MembersController::class, 'setAvatar']);
        Route::put('members/{member}', [MembersController::class,'update']);
        Route::post('partners/bySlug/{partner:slug}/photo', [PartnersController::class, 'setPhoto']);
        Route::put('partners/bySlug/{partner:slug}', [PartnersController::class,'update']);
        Route::post('partners/{partner}/photo', [PartnersController::class, 'setPhoto']);
        Route::put('partners/{partner}', [PartnersController::class,'update']);
    });

    Route::prefix('auth')->group(function() {
        Route::middleware('auth:sanctum')->post('login/admin', [AuthController::class, 'loginAsEmail']);
        Route::middleware('auth:sanctum')->post('logout', [AuthController::class, 'logout']);
        Route::post('login', [AuthController::class, 'login']);
        Route::post('send_reset_link', [AuthController::class, 'sendPasswordReset']);
        Route::post('reset_password', [AuthController::class, 'resetPassword']);
    });

    Route::prefix('members')->group(function(){
        Route::middleware('auth:sanctum')->group(function() {
            Route::post('{member}/avatar', [MembersController::class, 'setAvatar'])->where('member','[0-9]+');
            Route::post('avatar/{member}', [MembersController::class, 'setAvatar'])->where('member','[0-9]+');
            Route::get('{member}', [MembersController::class,'show'])->where('member','[0-9]+');
            Route::put('{member}', [MembersController::class,'update'])->where('member','[0-9]+');
            Route::get('self', [MembersController::class, 'showSelf']);

            Route::post('{member}/datastore', [MembersController::class, 'updateDatastore'])->where('member','[0-9]+');
            Route::get('{member}/datastore', [MembersController::class, 'getDatastore'])->where('member','[0-9]+');

            Route::post('{member}/subscriptions', [SubscriptionsController::class, 'store']);
            Route::delete('{member}/subscriptions/{subscription}', [SubscriptionsController::class, 'delete']);
            Route::put('{member}/subscriptions/{subscription}', [SubscriptionsController::class, 'update']);
            Route::get('{member}/subscriptions/{subscription}', [SubscriptionsController::class, 'show']);
            Route::get('{member}/subscriptions', [SubscriptionsController::class, 'list']);

            Route::get('{member}/family', [MembersController::class,'family'])->where('member','[0-9]+');

            Route::post('{member}/transactions', [TransactionsController::class, 'store']);
            Route::get('{member}/transactions/{transaction}', [TransactionsController::class, 'show']);
            Route::get('{member}/transactions', [TransactionsController::class, 'list']);
            Route::get('/', [MembersController::class, 'list']);
        });


        Route::prefix('vanity')->group(function(){
            Route::get('avatars/{count?}', [MembersVanityController::class,'getSampleAvatars']);
        });

        Route::put('testValidation/{member?}', [MembersController::class,'testValidation']);
        Route::put('checkEmail', [MembersController::class,'checkEmail']);
        Route::get('asGuest/{member}', [MembersController::class,'show'])->where('member','[0-9]+');
        Route::post('/', [MembersController::class,'store']);
    });

    // legacy validation test route
    Route::put('test/members/{member?}', [MembersController::class,'testValidation']);

    Route::prefix('goals')->group(function() {
        Route::middleware('auth:sanctum')->post('{project}/updates', [UpdatesController::class, 'store']);

        Route::get('{project}/updates', [UpdatesController::class, 'index']);
        Route::get('{project}', [ProjectsController::class, 'show']);
        Route::get('/', [ProjectsController::class, 'index']);
    });

    Route::prefix('updates')->group(function() {
        Route::get('{update}', [UpdatesController::class, 'show']);

        Route::middleware('auth:sanctum')->group(function () {
            Route::put('{update}', [UpdatesController::class, 'update']);
            Route::delete('{update}', [UpdatesController::class, 'destroy']);
        });
    });

    Route::prefix('partners')->group(function() {
        Route::get('/impactSummary', [PartnersController::class, 'fundSummary']);
        Route::get('bySlug/{partner:slug}', [PartnersController::class,'show']);
        Route::get('{partner}', [PartnersController::class,'show']);
        Route::get('/', [PartnersController::class,'list']);

        Route::middleware('auth:sanctum')->group(function() {
            Route::post('bySlug/{partner:slug}/photo', [PartnersController::class, 'setPhoto']);
            Route::put('bySlug/{partner:slug}', [PartnersController::class,'update']);
            Route::post('{partner}/photo', [PartnersController::class, 'setPhoto']);
            Route::put('{partner}', [PartnersController::class,'update']);
        });
    });

    Route::prefix('leaderboard')->group(function() {

        Route::prefix('partners')->group(function() {
            Route::get('monthly/{member}/{scope?}', [LeaderboardController::class, 'showByPartnerLocationMonthly']);
            Route::get('monthly', [LeaderboardController::class, 'showGlobalMonthlyPartners']);
            Route::get('{member}/{scope?}', [LeaderboardController::class, 'showByPartnerLocation']);
            Route::get('/', [LeaderboardController::class, 'showGlobalPartners']);
        });

        Route::prefix('members')->group(function() {
            Route::get('monthly/{member}/{scope?}', [LeaderboardController::class, 'showByMemberLocationMonthly']);
            Route::get('monthly', [LeaderboardController::class, 'showGlobalMonthlyMembers']);
            Route::get('{member}/{scope?}', [LeaderboardController::class, 'showByMemberLocation']);
            Route::get('/', [LeaderboardController::class, 'showGlobalMembers']);

        });

        Route::get('monthly/{member}/{scope?}', [LeaderboardController::class, 'showByMemberLocationMonthly']);
        Route::get('monthly', [LeaderboardController::class, 'showGlobalMonthlyMembers']);
        Route::get('{member}/{scope?}', [LeaderboardController::class, 'showByMemberLocation']);
        Route::get('/', [LeaderboardController::class, 'showGlobalMembers']);
    });

});
